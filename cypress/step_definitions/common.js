Then("I navigate to the registration page", () => {
  cy.visit("/registration");
});

Then("I fill in registration form as a new user", () => {
  cy.get(".test-title").click();
  cy.get(".test-titleMs").click();
  cy.get(".test-fullName input").type("Nurse Normal User");
  cy.get(".test-homeAddress input").first().type("31 Te Mai Road, Woodhill, Whangarei, New Zealand");
});
