import {
  POST_REGISTRATION,
  RECEIVE_REGISTRATION_RESPONSE,
  SET_REGISTRATION_STATUS_DIALOG,
  SET_LOGIN_STATUS_DIALOG,
  RECEIVE_LOGIN_RESPONSE,
  RECEIVE_LOGOUT_RESPONSE,
  REQUEST_PDRP_START,
  RECEIVE_PDRP_START,
  RECEIVE_PDRP_EXIST,
  RECEIVE_LOAD_COMPETENCIES_RESPONSE,
  SET_SAVE_COMPETENCY_STATUS_DIALOG,
  RECEIVE_SAVE_COMPETENCY_RESPONSE,
  SET_STATUS_DIALOG,
  SET_ERROR_MESSAGE,
  REQUEST_RESET_RECEIVED,
  RECEIVE_LOAD_EVIDENCES_RESPONSE,
  RECEIVE_CREATE_EVIDENCE_RESPONSE,
  RECEIVE_UPDATE_PDRP_PUNA,
  RECEIVE_UPDATE_COMPETENCY_PUNA,
  RECEIVE_PEER_REVIEWER,
  RECEIVE_PEER_PDRP_COMPLETE,
  RECEIVE_ALL_USERS,
  SET_ASSESSOR,
  RECEIVE_PEER_COMPLETE_PDRPS,
  ASSIGN_ASSESSOR,
  RECEIVE_ASSESSOR_ASSIGNED_PDRPS,
  RECEIVE_COMPETENCY_STANDARD
} from "../constants/constants";
import fetch from "isomorphic-fetch";

export function postRegistration() {
  return {
    type: POST_REGISTRATION
  };
}

export function receiveRegistationResponse(registrationDetails) {
  return {
    type: RECEIVE_REGISTRATION_RESPONSE,
    registrationDetails
  };
}

export function setRegistrationStatusDialog(dialogOpen) {
  return {
    type: SET_REGISTRATION_STATUS_DIALOG,
    dialogOpen
  };
}

export function requestRegistration(registrationDetails) {
  return function(dispatch) {
    dispatch(postRegistration(registrationDetails));
    dispatch(setStatusDialog(true, "Processing registration ..."));
    return fetch(`${process.env.REACT_APP_API_URL}/api/registration`, {
      body: JSON.stringify(registrationDetails),
      headers: {
        "content-type": "application/json"
      },
      method: "POST"
    })
      .then(function(response) {
        if (response.ok) {
          return response.json();
        }
        throw new Error("Network response was not ok.");
      })
      .then(responseJson => {
        dispatch(receiveRegistationResponse(responseJson));
        dispatch(setStatusDialog(false));
      })
      .catch(function(error) {
        console.log(
          "There has been a problem with your fetch operation: ",
          error.message
        );
        dispatch(
          setErrorMessage(
            true,
            "Sorry we can't register you right now. Please try again in a few minutes or contact us."
          )
        );
      });
    // .then(
    //   response => response.json(),
    //   error => console.log("An error occurred.", error)
    // )
    // .then(responseJson => {
    //   console.log(responseJson);
    // })
    // .then(json =>
    //   dispatch(receiveRegistationResponse(registrationDetails, json))
    // );
  };
}

export function setLoginStatusDialog(dialogOpen) {
  return {
    type: SET_LOGIN_STATUS_DIALOG,
    dialogOpen
  };
}

export function receiveLoginResponse(responseJson) {
  return {
    type: RECEIVE_LOGIN_RESPONSE,
    responseJson
  };
}

export function setErrorMessage(display, message) {
  return {
    type: SET_ERROR_MESSAGE,
    message,
    display
  };
}

export function requestLogin(loginDetails) {
  return function(dispatch) {
    dispatch(setStatusDialog(true, "Logging you in ..."));
    return fetch(`${process.env.REACT_APP_API_URL}/api/login`, {
      body: JSON.stringify(loginDetails),
      headers: {
        "content-type": "application/json"
      },
      method: "POST"
    })
      .then(function(response) {
        if (response.ok) {
          return response.json();
        }
        throw new Error("Network response was not ok.");
      })
      .then(responseJson => dispatch(receiveLoginResponse(responseJson)))
      .catch(function(error) {
        console.log(
          "There has been a problem with your fetch operation: ",
          error.message
        );
        dispatch(
          setErrorMessage(
            true,
            "Sorry we can't log you in now. Please make sure you entered the correct email and password (password is case sensitive) sent to you in your welcome email, or password reset email."
          )
        );
      });
  };
}

export function setPdrpStatusDialog(dialogOpen) {
  return {
    type: REQUEST_PDRP_START,
    dialogOpen
  };
}

export function receivePdrpResponse(responseJson) {
  return {
    type: RECEIVE_PDRP_START,
    responseJson
  };
}

export function requestPdrpStart(userId) {
  return function(dispatch) {
    dispatch(setPdrpStatusDialog(true));
    return fetch(`${process.env.REACT_APP_API_URL}/api/pdrp-start`, {
      body: JSON.stringify(userId),
      headers: {
        "content-type": "application/json"
      },
      method: "POST"
    })
      .then(
        response => response.json(),
        error => console.log("An error occurred.", error)
      )
      .then(responseJson => {
        dispatch(receivePdrpResponse(responseJson));
      });
  };
}

export function receivePdrpExistResponse(responseJson) {
  return {
    type: RECEIVE_PDRP_EXIST,
    responseJson
  };
}

export function requestPdrpExist(userId) {
  return function(dispatch) {
    dispatch(setStatusDialog(true));
    return fetch(`${process.env.REACT_APP_API_URL}/api/pdrp-exist`, {
      body: JSON.stringify(userId),
      headers: {
        "content-type": "application/json"
      },
      method: "POST"
    })
      .then(
        response => response.json(),
        error => console.log("An error occurred.", error)
      )
      .then(responseJson => {
        dispatch(receivePdrpExistResponse(responseJson));
        dispatch(setStatusDialog(false));
        dispatch(loadPeerReviewer({ api_token: userId.api_token }));
      });
  };
}

export function receiveLogoutResponse(responseJson) {
  return {
    type: RECEIVE_LOGOUT_RESPONSE,
    responseJson
  };
}

export function requestLogout(logoutDetails) {
  return function(dispatch) {
    return fetch(`${process.env.REACT_APP_API_URL}/api/logout`, {
      body: JSON.stringify(logoutDetails),
      headers: {
        "content-type": "application/json"
      },
      method: "POST"
    })
      .then(
        response => response.json(),
        error => console.log("An error occurred.", error)
      )
      .then(responseJson => {
        dispatch(receiveLogoutResponse(responseJson));
      });
  };
}

export function receiveLoadCompetenciesResponse(responseJson) {
  return {
    type: RECEIVE_LOAD_COMPETENCIES_RESPONSE,
    responseJson
  };
}

export function loadCompetencies(userId) {
  return function(dispatch) {
    //dispatch(setStatusDialog(true));
    return fetch(`${process.env.REACT_APP_API_URL}/api/competencies-load`, {
      body: JSON.stringify(userId),
      headers: {
        "content-type": "application/json"
      },
      method: "POST"
    })
      .then(
        response => response.json(),
        error => console.log("An error occurred.", error)
      )
      .then(responseJson => {
        dispatch(receiveLoadCompetenciesResponse(responseJson));
        dispatch(setStatusDialog(false));
        dispatch(loadPeerReviewer({ api_token: userId.api_token }));
      });
  };
}

export function setSaveCompetencyStatusDialog(dialog) {
  return {
    type: SET_SAVE_COMPETENCY_STATUS_DIALOG,
    dialog
  };
}

export function receiveSaveCompetencyResponse(responseJson) {
  return {
    type: RECEIVE_SAVE_COMPETENCY_RESPONSE,
    responseJson
  };
}

export function saveCompetency(content) {
  return function(dispatch) {
    dispatch(setStatusDialog(true, "Saving Competency ..."));
    return fetch(`${process.env.REACT_APP_API_URL}/api/competency-save`, {
      body: JSON.stringify(content),
      headers: {
        "content-type": "application/json"
      },
      method: "POST"
    })
      .then(
        response => response.json(),
        error => console.log("An error occurred.", error)
      )
      .then(responseJson => {
        dispatch(receiveSaveCompetencyResponse(responseJson));
        dispatch(setStatusDialog(false, ""));
      });
  };
}

export function setStatusDialog(dialog, message) {
  return {
    type: SET_STATUS_DIALOG,
    dialog,
    message
  };
}

export function requestResetReceived() {
  return {
    type: REQUEST_RESET_RECEIVED
  };
}

export function requestReset(email) {
  return function(dispatch) {
    dispatch(setStatusDialog(true, "Resetting password ..."));
    return fetch(`${process.env.REACT_APP_API_URL}/api/reset-password`, {
      body: JSON.stringify(email),
      headers: {
        "content-type": "application/json"
      },
      method: "POST"
    })
      .then(function(response) {
        if (response.ok) {
          return response.json();
        }
        throw new Error("Network response was not ok.");
      })
      .then(responseJson => dispatch(requestResetReceived()))
      .catch(function(error) {
        console.log(
          "There has been a problem with your fetch operation: ",
          error.message
        );
        dispatch(
          setErrorMessage(
            true,
            "Sorry we can't reset your password. Please try again in a few minutes or contact us."
          )
        );
      });
  };
}

export function loadEvidences(userId) {
  return function(dispatch) {
    //dispatch(setStatusDialog(true));
    return fetch(`${process.env.REACT_APP_API_URL}/api/evidences-load`, {
      body: JSON.stringify(userId),
      headers: {
        "content-type": "application/json"
      },
      method: "POST"
    })
      .then(
        response => response.json(),
        error => console.log("An error occurred.", error)
      )
      .then(responseJson => {
        dispatch(receiveLoadEvidencesResponse(responseJson));
        dispatch(setStatusDialog(false));
      });
  };
}

export function receiveLoadEvidencesResponse(responseJson) {
  return {
    type: RECEIVE_LOAD_EVIDENCES_RESPONSE,
    responseJson
  };
}

export function receiveCreateEvidenceResponse(responseJson) {
  return {
    type: RECEIVE_CREATE_EVIDENCE_RESPONSE,
    responseJson
  };
}

export function createEvidence(content) {
  return function(dispatch) {
    dispatch(setStatusDialog(true, "Creating Evidence Record ..."));
    return fetch(`${process.env.REACT_APP_API_URL}/api/evidence-create`, {
      body: JSON.stringify(content),
      headers: {
        "content-type": "application/json"
      },
      method: "POST"
    })
      .then(
        response => response.json(),
        error => console.log("An error occurred.", error)
      )
      .then(responseJson => {
        dispatch(receiveCreateEvidenceResponse(responseJson));
        dispatch(setStatusDialog(false, ""));
      });
  };
}

export function receiveUpdatePdrpPuna(responseJson) {
  return {
    type: RECEIVE_UPDATE_PDRP_PUNA,
    responseJson
  };
}

export function updatePdrpPuna(content) {
  return function(dispatch) {
    dispatch(setStatusDialog(true, "Updating Puna ..."));
    return fetch(`${process.env.REACT_APP_API_URL}/api/puna-update`, {
      body: JSON.stringify(content),
      headers: {
        "content-type": "application/json"
      },
      method: "POST"
    })
      .then(
        response => response.json(),
        error => console.log("An error occurred.", error)
      )
      .then(responseJson => {
        dispatch(receiveUpdatePdrpPuna(responseJson));
        dispatch(setStatusDialog(false, ""));
      });
  };
}

export function requestPeerReviewer(content) {
  return function(dispatch) {
    dispatch(setStatusDialog(true, "Sending request to Peer Reviewer ..."));
    return fetch(`${process.env.REACT_APP_API_URL}/api/peer-add`, {
      body: JSON.stringify(content),
      headers: {
        "content-type": "application/json"
      },
      method: "POST"
    })
      .then(
        response => response.json(),
        error => console.log("An error occurred.", error)
      )
      .then(responseJson => {
        dispatch(setStatusDialog(false, ""));
      });
  };
}

export function receiveloadPeerReviewer(responseJson) {
  return {
    type: RECEIVE_PEER_REVIEWER,
    responseJson
  };
}

export function loadPeerReviewer(apiToken) {
  return function(dispatch) {
    return fetch(`${process.env.REACT_APP_API_URL}/api/peer-get`, {
      body: JSON.stringify(apiToken),
      headers: {
        "content-type": "application/json"
      },
      method: "POST"
    })
      .then(
        response => response.json(),
        error => console.log("An error occurred.", error)
      )
      .then(responseJson => {
        dispatch(receiveloadPeerReviewer(responseJson));
      });
  };
}

export function receivePeerPdrpComplete(responseJson) {
  return {
    type: RECEIVE_PEER_PDRP_COMPLETE
  };
}

export function peerPdrpComplete(content) {
  return function(dispatch) {
    dispatch(setStatusDialog(true, "Marking as complete ..."));
    return fetch(`${process.env.REACT_APP_API_URL}/api/peer-pdrp-complete`, {
      body: JSON.stringify(content),
      headers: {
        "content-type": "application/json"
      },
      method: "POST"
    })
      .then(
        response => response.json(),
        error => console.log("An error occurred.", error)
      )
      .then(responseJson => {
        dispatch(receivePeerPdrpComplete(responseJson));
        dispatch(setStatusDialog(false, ""));
      });
  };
}

export function receiveAllUsers(responseJson) {
  return {
    type: RECEIVE_ALL_USERS,
    responseJson
  };
}

export function getAllUsers(apiToken) {
  return function(dispatch) {
    return fetch(`${process.env.REACT_APP_API_URL}/api/all-users`, {
      body: JSON.stringify(apiToken),
      headers: {
        "content-type": "application/json"
      },
      method: "POST"
    })
      .then(
        response => response.json(),
        error => console.log("An error occurred.", error)
      )
      .then(responseJson => {
        dispatch(receiveAllUsers(responseJson));
      });
  };
}

export function completeSetAssessor(responseJson) {
  return {
    type: SET_ASSESSOR
  };
}

export function setAssessor(asessor) {
  return function(dispatch) {
    return fetch(`${process.env.REACT_APP_API_URL}/api/set-assessor`, {
      body: JSON.stringify(asessor),
      headers: {
        "content-type": "application/json"
      },
      method: "POST"
    })
      .then(
        response => response.json(),
        error => console.log("An error occurred.", error)
      )
      .then(responseJson => {
        dispatch(completeSetAssessor(responseJson));
        dispatch(getAllUsers({ api_token: asessor.api_token }));
      });
  };
}

export function completeAssignAssessor(responseJson) {
  return {
    type: ASSIGN_ASSESSOR
  };
}

export function assignAssessor(assignAssessorData) {
  return function(dispatch) {
    return fetch(`${process.env.REACT_APP_API_URL}/api/assign-assessor`, {
      body: JSON.stringify(assignAssessorData),
      headers: {
        "content-type": "application/json"
      },
      method: "POST"
    })
      .then(
        response => response.json(),
        error => console.log("An error occurred.", error)
      )
      .then(responseJson => {
        dispatch(completeAssignAssessor(responseJson));
      });
  };
}

export function receivePeerCompletePdrps(responseJson) {
  return {
    type: RECEIVE_PEER_COMPLETE_PDRPS,
    responseJson
  };
}

export function getPeerCompletePdrps(apiToken) {
  return function(dispatch) {
    return fetch(`${process.env.REACT_APP_API_URL}/api/peer-complete-pdrps`, {
      body: JSON.stringify(apiToken),
      headers: {
        "content-type": "application/json"
      },
      method: "POST"
    })
      .then(
        response => response.json(),
        error => console.log("An error occurred.", error)
      )
      .then(responseJson => {
        dispatch(receivePeerCompletePdrps(responseJson));
      });
  };
}

export function receiveAssessorAssignedPdrps(responseJson) {
  return {
    type: RECEIVE_ASSESSOR_ASSIGNED_PDRPS,
    responseJson
  };
}

export function getAssessorAssignedPdrps(apiToken) {
  return function(dispatch) {
    return fetch(
      `${process.env.REACT_APP_API_URL}/api/get-assigned-assessors`,
      {
        body: JSON.stringify(apiToken),
        headers: {
          "content-type": "application/json"
        },
        method: "POST"
      }
    )
      .then(
        response => response.json(),
        error => console.log("An error occurred.", error)
      )
      .then(responseJson => {
        dispatch(receiveAssessorAssignedPdrps(responseJson));
      });
  };
}

export function receiveupdateCompetencyPuna(responseJson) {
  return {
    type: RECEIVE_UPDATE_COMPETENCY_PUNA,
    responseJson
  };
}

export function updateCompetencyPuna(content) {
  return function(dispatch) {
    dispatch(setStatusDialog(true, "Updating Puna ..."));
    return fetch(
      `${process.env.REACT_APP_API_URL}/api/puna-competency-update`,
      {
        body: JSON.stringify(content),
        headers: {
          "content-type": "application/json"
        },
        method: "POST"
      }
    )
      .then(
        response => response.json(),
        error => console.log("An error occurred.", error)
      )
      .then(responseJson => {
        dispatch(receiveupdateCompetencyPuna(responseJson));
        dispatch(setStatusDialog(false, ""));
      });
  };
}

export function receiveCompetencyStandard(responseJson) {
  return {
    type: RECEIVE_COMPETENCY_STANDARD
  };
}

export function setCompetencyStandard(content) {
  return function(dispatch) {
    return fetch(`${process.env.REACT_APP_API_URL}/api/set-met`, {
      body: JSON.stringify(content),
      headers: {
        "content-type": "application/json"
      },
      method: "POST"
    })
      .then(
        response => response.json(),
        error => console.log("An error occurred.", error)
      )
      .then(responseJson => {
        dispatch(receiveCompetencyStandard(responseJson));
      });
  };
}
