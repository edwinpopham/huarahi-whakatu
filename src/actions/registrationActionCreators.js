import { SET_TITLE, SET_FULLNAME } from "../constants/registrationConstants";

export function setTitle(title) {
	return dispatch => {
		dispatch(setTitleAsync(title));
	};
}

function setTitleAsync(title) {
	return {
		type: SET_TITLE,
		title: title
	};
}

export function setFullName(fullName) {
	return dispatch => {
		dispatch(setFullNameAsync(fullName));
	};
}

function setFullNameAsync(fullName) {
	return {
		type: SET_FULLNAME,
		fullName: fullName
	};
}
