// @flow
import React, { Component } from "react";
import injectSheet from "react-jss";
import Heading from "./Heading";
import AdminNav from "./AdminNav";
import AdminUser from "./AdminUser";
import { connect } from "react-redux";
import { getAllUsers } from "../actions/actionCreators";

const classStyles = (theme: JssTheme) => ({
  userTable: {
    width: "100%"
  },
  tableHeader: {
    textAlign: "left"
  }
});

export class AdminAllUsers extends Component {
  componentDidMount() {
    const { getAllUsers, apiToken } = this.props;
    getAllUsers({ api_token: apiToken });
  }
  render() {
    const { classes, users } = this.props;
    const listUsers = users.map((user, index) => (
      <AdminUser key={index} user={user} />
    ));

    return (
      <div>
        <AdminNav />
        <Heading>All Users</Heading>
        <p>Click the checkbox to make a user an assessor.</p>
        <table className={classes.userTable}>
          <tbody>
            <tr className={classes.tableHeader}>
              <th>Name</th>
              <th>Email</th>
              <th>Last Login</th>
              <th>Joined Huarahi</th>
              <th>Is Assessor</th>
              <th>PDRP</th>
            </tr>
            {listUsers}
          </tbody>
        </table>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    apiToken: state.login.loginStatus.response.data.api_token,
    users: state.admin.users
  };
};
const mapDispatchToProps = dispatch => ({
  getAllUsers(apiToken) {
    dispatch(getAllUsers(apiToken));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectSheet(classStyles)(AdminAllUsers));
