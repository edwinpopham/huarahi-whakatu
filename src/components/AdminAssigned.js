// @flow
import React, { Component } from "react";
import injectSheet from "react-jss";
import Heading from "./Heading";
import AdminNav from "./AdminNav";
import { connect } from "react-redux";
import { getAssessorAssignedPdrps } from "../actions/actionCreators";

const classStyles = (theme: JssTheme) => ({});

export class AdminAssigned extends Component {
  componentDidMount() {
    const { getAssessorAssignedPdrps, apiToken } = this.props;
    getAssessorAssignedPdrps({ api_token: apiToken });
  }
  render() {
    const { classes, assessorAssignedPdrps } = this.props;
    const listassessorAssignedPdrps = assessorAssignedPdrps.map(
      (assessorAssignedPdrp, index) => {
        return (
          <li key={index}>{`${
            assessorAssignedPdrp.pdrp.user.registration.first_name
          } ${
            assessorAssignedPdrp.pdrp.user.registration.surname
          } - is being assessed by - ${
            assessorAssignedPdrp.user.registration.first_name
          } ${assessorAssignedPdrp.user.registration.surname}`}</li>
        );
      }
    );

    return (
      <div>
        <AdminNav />
        <Heading>Assigned PDRP's</Heading>
        <ul>{listassessorAssignedPdrps}</ul>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    apiToken: state.login.loginStatus.response.data.api_token,
    assessorAssignedPdrps: state.admin.assessorAssignedPdrps
  };
};
const mapDispatchToProps = dispatch => ({
  getAssessorAssignedPdrps(apiToken) {
    dispatch(getAssessorAssignedPdrps(apiToken));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectSheet(classStyles)(AdminAssigned));
