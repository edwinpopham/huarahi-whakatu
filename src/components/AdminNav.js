// @flow
import React, { Component } from "react";
import injectSheet from "react-jss";
import { Link } from "react-router-dom";
import RaisedButton from "material-ui/RaisedButton";

const classStyles = (theme: JssTheme) => ({});

export class AdminNav extends Component {
  render() {
    const { classes } = this.props;

    return (
      <div>
        <RaisedButton
          label="All Users"
          secondary={true}
          style={{marginRight: '10px'}}
          containerElement={<Link to="/all-users" />}
        />
        <RaisedButton
          label="Assign Assessor"
          secondary={true}
          style={{marginRight: '10px'}}
          containerElement={<Link to="/admin-ready" />}
        />
        <RaisedButton
          label="Assessor Assigned"
          secondary={true}
          containerElement={<Link to="/admin-assigned" />}
        />
      </div>
    );
  }
}

export default injectSheet(classStyles)(AdminNav);
