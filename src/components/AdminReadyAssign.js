// @flow
import React, { Component } from "react";
import injectSheet from "react-jss";
import WarningIcon from "material-ui/svg-icons/alert/warning";
import Heading from "./Heading";
import AdminNav from "./AdminNav";
import AdminReadyAssignDropdown from "./AdminReadyAssignDropdown";
import { connect } from "react-redux";
import { getAllUsers, getPeerCompletePdrps } from "../actions/actionCreators";

const classStyles = (theme: JssTheme) => ({});

export class AdminReadyAssign extends Component {
  constructor(props) {
    super(props);
    this.state = { value: 1 };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event, index, value) {
    this.setState({ value });
  }

  componentDidMount() {
    const { getAllUsers, getPeerCompletePdrps, apiToken } = this.props;
    getAllUsers({ api_token: apiToken });
    getPeerCompletePdrps({ api_token: apiToken });
  }

  render() {
    const { classes, users, peerCompletePdrps } = this.props;

    const assessors = users.filter(
      user => user.user.roles === 6 || user.user.roles === 4
    );
    const listItems = peerCompletePdrps.map(peerCompletePdrp => (
      <li key={peerCompletePdrp.id}>
        PDRP ID: {peerCompletePdrp.id},{" "}
        {peerCompletePdrp.user.registration.first_name +
          " " +
          peerCompletePdrp.user.registration.surname +
          ", " +
          peerCompletePdrp.type.name}{" "}
        <AdminReadyAssignDropdown
          assessors={assessors}
          nurseId={peerCompletePdrp.user.id}
          pdrpId={peerCompletePdrp.id}
        />
      </li>
    ));
    return (
      <div>
        <AdminNav />
        <Heading>Ready to Assign Assessor</Heading>
        <p>
          These pdrps have been marked as complete by a peer assessor. Choose an
          assessor from the dropdown box to assign to one to the pdrp.
        </p>
        {listItems && <ul>{listItems}</ul>}
        {listItems.length < 1 && (
          <p>
            <WarningIcon /> No PDRP's marked as ready for an assessor right now.
          </p>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    apiToken: state.login.loginStatus.response.data.api_token,
    users: state.admin.users,
    peerCompletePdrps: state.admin.peerCompletePdrps
  };
};
const mapDispatchToProps = dispatch => ({
  getAllUsers(apiToken) {
    dispatch(getAllUsers(apiToken));
  },
  getPeerCompletePdrps(apiToken) {
    dispatch(getPeerCompletePdrps(apiToken));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectSheet(classStyles)(AdminReadyAssign));
