// @flow
import React, { Component } from "react";
import injectSheet from "react-jss";
import SelectField from "material-ui/SelectField";
import MenuItem from "material-ui/MenuItem";
import { connect } from "react-redux";
import { assignAssessor } from "../actions/actionCreators";

const classStyles = (theme: JssTheme) => ({});

export class AdminReadyAssignDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = { value: 1 };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event, index, value) {
    const { apiToken, nurseId, assignAssessor , pdrpId} = this.props;
    this.setState({ value });
    assignAssessor({
      api_token: apiToken,
      pdrp_id: pdrpId,
      assessor_id: value
    });
  }

  render() {
    const { classes, users, peerCompletePdrps, assessors } = this.props;
    const listItems = assessors.map(assessor => (
      <MenuItem
        key={assessor.user_id}
        value={assessor.user_id}
        primaryText={assessor.first_name + " " + assessor.surname}
      />
    ));

    return (
      <SelectField
        floatingLabelText="Assessor"
        value={this.state.value}
        onChange={this.handleChange}
      >
        {listItems}
      </SelectField>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    apiToken: state.login.loginStatus.response.data.api_token,
    users: state.admin.users,
    peerCompletePdrps: state.admin.peerCompletePdrps
  };
};
const mapDispatchToProps = dispatch => ({
  assignAssessor(assignAssessorData) {
    dispatch(assignAssessor(assignAssessorData));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectSheet(classStyles)(AdminReadyAssignDropdown));
