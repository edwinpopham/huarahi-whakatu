// @flow
import React, { Component } from "react";
import Checkbox from "material-ui/Checkbox";
import injectSheet from "react-jss";
import moment from "moment";
import RaisedButton from "material-ui/RaisedButton";
import localization from "moment/locale/en-nz";
import { connect } from "react-redux";
import { setAssessor } from "../actions/actionCreators";

moment.locale("en-nz", localization);

const classStyles = (theme: JssTheme) => ({
  adminUser: {
    "&:hover": {
      backgroundColor: "#EEEEEE",
    },
  },
  cell: {
    borderTop: "1px solid #CCCCCC",
  },
});

export class AdminUser extends Component {
  render() {
    const { classes, user, setAssessor, apiToken } = this.props;
    const isAssessor = user.user.roles === 6 ? true : false;
    const pdrpUrl = `${process.env.REACT_APP_API_URL}/api/pdrp-report?apiToken=${apiToken}&nurseId=${user.user.id}`;

    return (
      <tr className={classes.adminUser}>
        <td className={classes.cell}>
          {user.first_name} {user.surname}
        </td>
        <td className={classes.cell}>
          <a href={"mailto:" + user.user.email}>{user.user.email}</a>
        </td>
        <td className={classes.cell}>
          {moment(user.user.updated_at).format("L")}
        </td>
        <td className={classes.cell}>
          {moment(user.user.created_at).format("L")}
        </td>
        <td className={classes.cell}>
          {" "}
          <Checkbox
            checked={isAssessor}
            onCheck={() => setAssessor({ user, api_token: apiToken })}
          />
        </td>
        <td>
          {" "}
          <RaisedButton
            href={pdrpUrl}
            label="view"
            primary={true}
            target="_blank"
          />
        </td>
      </tr>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    apiToken: state.login.loginStatus.response.data.api_token,
  };
};
const mapDispatchToProps = (dispatch) => ({
  setAssessor(asessor) {
    dispatch(setAssessor(asessor));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectSheet(classStyles)(AdminUser));
