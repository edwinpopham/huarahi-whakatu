import React, { Component } from "react";
import injectSheet from "react-jss";
import WarningIcon from "material-ui/svg-icons/alert/warning";

const classStyles = (theme: JssTheme) => ({
  alert: {
    backgroundColor: theme.palette.alertWarning,
    padding: "15px 20px"
  },
  icon: {
    verticalAlign: "bottom",
    marginRight: "15px"
  }
});

class Alert extends Component {
  render() {
    const { classes, children } = this.props;

    return (
      <div className={classes.alert}>
        <WarningIcon className={classes.icon} />
        {children}
      </div>
    );
  }
}

export default injectSheet(classStyles)(Alert);
