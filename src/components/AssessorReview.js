// @flow
import React, { Component } from "react";
import injectSheet from "react-jss";
import TextField from "material-ui/TextField";
import RaisedButton from "material-ui/RaisedButton";
import { saveCompetency } from "../actions/actionCreators";
import { connect } from "react-redux";

const classStyles = (theme: JssTheme) => ({
  review: {
    paddingBottom: '20px',
    marginBottom: '20px'
  },
  description: {
    fontWeight: 'bold',
    marginBottom: '0px'
  }
});

export class AssessorReview extends Component {
  constructor(props) {
    super(props);
    const { competencyData } = this.props;
    this.state = {
      content: competencyData.comments[2].content
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({
      content: event.target.value
    });
  }

  render() {
    const {
      classes,
      number,
      competencyData,
      saveCompetency,
      apiToken,
      description
    } = this.props;

    return (
      <div className={classes.review}>
        <div>
          <p className={classes.description}>{number + " " + description}</p>
          <TextField
            multiLine={true}
            fullWidth={true}
            rows={5}
            rowsMax={5}
            defaultValue={this.state.content}
            onChange={this.handleChange}
            hintText={number + ' Please supply a supporting comment of how the nurse has met this competency.'}
            underlineShow={false}
            textareaStyle={{border: '1px solid #DEDEDE', padding: '10px'}}
            hintStyle={{padding: '10px', top: '12px'}}
          />
        </div>
        <div>
          <RaisedButton
            label="Save"
            secondary={true}
            style={{ "marginRight": "10px" }}
            onClick={event => {
              saveCompetency({
                content: this.state.content,
                api_token: apiToken,
                comment_id: competencyData.comments[2].id
              });
            }}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    apiToken: state.login.loginStatus.response.data.api_token
  };
};
const mapDispatchToProps = dispatch => ({
  saveCompetency(content) {
    dispatch(saveCompetency(content));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(
  injectSheet(classStyles)(AssessorReview)
);
