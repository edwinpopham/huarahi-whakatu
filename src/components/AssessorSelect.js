// @flow
import React, { Component } from "react";
import injectSheet from "react-jss";
import SelectField from "material-ui/SelectField";
import MenuItem from "material-ui/MenuItem";

const classStyles = (theme: JssTheme) => ({});

export class AssessorSelect extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: null
    };
  }

  handleChange(event, index, value) {
    this.setState({ value });
  }

  render() {
    const { value } = this.props;

    return (
      <SelectField
        floatingLabelText="Assessor"
        value={value ? value : this.state.value}
        onChange={this.handleChange.bind(this)}
        disabled={value ? true : false}
      >
        <MenuItem value={1} primaryText="Jane Doe" />
        <MenuItem value={2} primaryText="John Doe" />
        <MenuItem value={3} primaryText="Mark Jones" />
        <MenuItem value={4} primaryText="Sam Smith" />
        <MenuItem value={5} primaryText="Jeff James" />
      </SelectField>
    );
  }
}

export default injectSheet(classStyles)(AssessorSelect);
