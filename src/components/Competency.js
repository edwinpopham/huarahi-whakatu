// @flow
import React, { Component, Fragment } from "react";
import injectSheet from "react-jss";
import Review from "./Review";
import PeerReview from "./PeerReview";
import AssessorReview from "./AssessorReview";
import CompetencyStandard from "./CompetencyStandard";
import { connect } from "react-redux";

const classStyles = (theme: JssTheme) => ({
  comments: {},
  comment: {}
});

export class Competency extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 1
    };
  }

  render() {
    const {
      classes,
      title,
      number,
      competencyData,
      description,
      pdrpToReview,
      disabled
    } = this.props;
    const hide = true;
    const isAssessorReady = competencyData.comments.length === 3 ? true : false;
    return (
      <div className={classes.comments}>
        {!pdrpToReview && (
          <div className={classes.comment}>
            <Review
              title={title}
              number={number}
              competencyData={competencyData}
              description={description}
              disabled={disabled ? true : false}
            />
          </div>
        )}
        {pdrpToReview &&
          !isAssessorReady && (
            <Fragment>
              <div className={classes.comment}>
                <Review
                  title={title}
                  number={number}
                  competencyData={competencyData}
                  description={description}
                  disabled={true}
                />
              </div>
              <div className={classes.comment}>
                <PeerReview
                  title={title}
                  number={number}
                  competencyData={competencyData}
                  description={"Peer Review"}
                />
              </div>
            </Fragment>
          )}
        {pdrpToReview &&
          isAssessorReady && (
            <Fragment>
              <div className={classes.comment}>
                <Review
                  title={title}
                  number={number}
                  competencyData={competencyData}
                  description={description}
                  disabled={true}
                />
              </div>
              <div className={classes.comment}>
                <PeerReview
                  title={title}
                  number={number}
                  competencyData={competencyData}
                  description={"Peer Review"}
                  disabled={true}
                />
              </div>
              <div className={classes.comment}>
                <AssessorReview
                  title={title}
                  number={number}
                  competencyData={competencyData}
                  description={"Assessor Review"}
                />
                <CompetencyStandard
                  competencyId={competencyData.id}
                  competencyMet={competencyData.met}
                  competencyPdrp={competencyData.pdrp_id}
                />
              </div>
            </Fragment>
          )}
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    //registrationStatus: state.registration.registrationStatus.status
  };
};
const mapDispatchToProps = dispatch => ({
  // requestRegistration(registrationDetails) {
  //   dispatch(requestRegistration(registrationDetails));
  // }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectSheet(classStyles)(Competency));
