// @flow
import React, { Component } from "react";
import injectSheet from "react-jss";
import {
  Card,
  CardTitle,
  CardText
} from "material-ui/Card";
import { connect } from "react-redux";

const classStyles = (theme: JssTheme) => ({});

export class CompetencyGroup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 1
    };
  }

  render() {
    const { title, subtitle, children } = this.props;
    return (
      <Card>
        <CardTitle
          title={title}
          subtitle={subtitle}
          showExpandableButton={true}
          actAsExpander={true}
        />
        <CardText expanded="false" expandable={true}>
          {children}
        </CardText>
      </Card>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    //registrationStatus: state.registration.registrationStatus.status
  };
};
const mapDispatchToProps = dispatch => ({
  // requestRegistration(registrationDetails) {
  //   dispatch(requestRegistration(registrationDetails));
  // }
});

export default connect(mapStateToProps, mapDispatchToProps)(
  injectSheet(classStyles)(CompetencyGroup)
);

          // <SelectField
          //   floatingLabelText="Puna"
          //   value={this.state.value}
          //   onChange={this.handleChange}
          // >
          //   <MenuItem value={1} primaryText="Whakato" />
          //   <MenuItem value={2} primaryText="Whakatau" />
          //   <MenuItem value={3} primaryText="Rahi" />
          //   <MenuItem value={4} primaryText="Rangatira" />
          // </SelectField>
