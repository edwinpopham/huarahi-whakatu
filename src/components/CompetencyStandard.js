// @flow
import React, { Component, Fragment } from "react";
import injectSheet from "react-jss";
import SelectField from "material-ui/SelectField";
import MenuItem from "material-ui/MenuItem";
import { setCompetencyStandard } from "../actions/actionCreators";
import { connect } from "react-redux";

const classStyles = (theme: JssTheme) => ({});

export class CompetencyStandard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: props.competencyMet
    };
  }

  handleChange = (event, key, payload) => {
    const { competencyId, apiToken, competencyPdrp, setCompetencyStandard } = this.props;
    this.setState({
      value: payload
    });
    setCompetencyStandard({
      competency_id: competencyId,
      api_token: apiToken,
      pdrp_id: competencyPdrp,
      met: payload
    })
  };

  render() {
    const { classes } = this.props;

    return (
      <SelectField
        floatingLabelText="Met or Not Met"
        value={this.state.value}
        onChange={(event, key, payload) =>
          this.handleChange(event, key, payload)
        }
      >
        <MenuItem value={0} primaryText="Not Met" />
        <MenuItem value={1} primaryText="Met" />
      </SelectField>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    apiToken: state.login.loginStatus.response.data.api_token
  };
};
const mapDispatchToProps = dispatch => ({
  setCompetencyStandard(content) {
    dispatch(setCompetencyStandard(content));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectSheet(classStyles)(CompetencyStandard));
