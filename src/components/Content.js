import React from "react";
import injectSheet from "react-jss";
import Paper from "material-ui/Paper";
import { APPLY_AT_LARGE_SIZE } from "../constants/breakPoints";

const classStyles = (theme: JssTheme) => ({
  content: {
    fontFamily: theme.fontFamily,
    margin: "74px 10px 20px 10px",
    [APPLY_AT_LARGE_SIZE]: {
      margin: "94px 20px 20px 20px"
    }
  }
});

class Content extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.content}>
        <Paper style={{ padding: "20px" }} zDepth={1}>
          {this.props.children}
        </Paper>
      </div>
    );
  }
}

export default injectSheet(classStyles)(Content);
