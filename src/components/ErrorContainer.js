// @flow
import React, { Component } from "react";
import injectSheet from "react-jss";
import { connect } from "react-redux";

const classStyles = (theme: JssTheme) => ({});

export class ErrorContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 1
    };
  }

  render() {
    const { errorMessageDisplay, errorMessage } = this.props;
    return <div>{errorMessageDisplay && <div> {errorMessage} </div>}</div>;
  }
}

const mapStateToProps = (state, props) => {
  return {
    errorMessageDisplay: state.errorMessage.display,
    errorMessage: state.errorMessage.message
  };
};
const mapDispatchToProps = dispatch => ({
  // requestRegistration(registrationDetails) {
  //   dispatch(requestRegistration(registrationDetails));
  // }
});

export default connect(mapStateToProps, mapDispatchToProps)(
  injectSheet(classStyles)(ErrorContainer)
);
