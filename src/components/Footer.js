import React from "react";
import injectSheet from "react-jss";
import RaisedButton from "material-ui/RaisedButton";
import FlatButton from "material-ui/FlatButton";

const classStyles = (theme: JssTheme) => ({
  footer: {
    fontFamily: theme.fontFamily,
    textAlign: "center",
    margin: "30px 0"
  },
  link: {
    color: theme.palette.primary1Color,
    textDecoration: "none"
  }
});

class Footer extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.footer}>
        <FlatButton
          label="Nurses Toolkit"
          primary={true}
          href="/nurses_tool-kit_handbook.pdf"
          target="blank"
        />
        <FlatButton
          label="Declaration"
          primary={true}
          href="/declaration.pdf"
          target="blank"
        />
        <p>
          Huarahi Whakatū by{" "}
          <a
            href="http://terauora.com"
            target="_blank"
            rel="noopener noreferrer"
            className={classes.link}
          >
            Te Rau Ora
          </a>
        </p>
        <a href="http://terauora.com" target="_blank" rel="noopener noreferrer">
          <img src="/terauora.png" alt="Te Rau Ora" />
        </a>
      </div>
    );
  }
}

export default injectSheet(classStyles)(Footer);
