import React from "react";
import injectSheet from "react-jss";
import { APPLY_AT_LARGE_SIZE } from "../constants/breakPoints";

const classStyles = (theme: JssTheme) => ({
  heading: {
    fontWeight: "400",
    fontSize: "1.2em",
    lineHeight: "1.4em",
    [APPLY_AT_LARGE_SIZE]: {
      fontSize: "1.6em",
      lineHeight: "1.8em"
    }
  },
  "@global h1": {
    marginTop: "0px"
  }
});

class Heading extends React.Component {
  render() {
    const { classes } = this.props;
    return <h1 className={classes.heading}>{this.props.children}</h1>;
  }
}

export default injectSheet(classStyles)(Heading);
