import React, { Component, Fragment } from "react";
import injectSheet from "react-jss";
import Dialog from "material-ui/Dialog";

const classStyles = (theme: JssTheme) => ({
  punaLink: {
    padding: "5px",
    color: theme.palette.primary1Color,
    cursor: "pointer",
    margin: "20px 0",
    display: "block"
  }
});

class HowToChoosePuna extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false
    };
  }

  handleOpen = () => {
    this.setState({
      open: true
    });
  };

  handleClose = () => {
    this.setState({
      open: false
    });
  };

  render() {
    const { classes } = this.props;
    return (
      <Fragment>
        <a onClick={this.handleOpen} className={classes.punaLink}>
          How to choose your puna
        </a>
        <Dialog
          title="How to choose your Puna"
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
        >
          <p>The term Puna represent levels or stages.</p>
          <h3>Puna Whakato - Competent Nurse</h3>
          <p>
            Puna Whakato may also be interpreted as a competent Māori nurse who
            is expected to be able to integrate theoretical knowledge and
            research with practice experience. There is an increasing awareness
            of issues represented by tangata whaiora and whānau responses in
            recurrent situations. Māori nurses practicing at this level should
            demonstrate sound ability in relevant technical skills and Pukenga
            Māori Motuhake. They should be very familiar with supporting
            situations with a dedicated tangata whaiora and whānau focus and are
            expected to be able to identify significant problems in a holistic
            way utilising both clinical and cultural models of practice and
            understandings.
          </p>
          <p>They will:</p>
          <ul>
            <li>
              effectively support tangata whaiora and whānau with predictable
              needs;
            </li>
            <li>
              be able to place presenting symptoms and issues within a broader
              context;
            </li>
            <li>
              seek assistance on aspects of care from more experienced
              colleagues;
            </li>
            <li>
              be able to plan and decide which aspects of care are more
              important and which are less relevant. The planning will begin to
              be based on prior knowledge and experience;
            </li>
            <li>
              demonstrate Pukenga Māori Motuhake daily in practice, increasing
              clinical understanding, enhanced technical skills, improved
              organisational ability, and a feeling of confidence;
            </li>
            <li>
              understand the relationship between nursing care and tangata
              whaiora and whānau responses and the wider determinants of Māori
              health;
            </li>
            <li>
              develop effective time management skills and begin to prioritise
              tangata whaiora and whānau work appropriately; and
            </li>
            <li>actively participate in supporting student nurses.</li>
          </ul>
          <h3>Puna Whakatau - Proficient Nurse</h3>
          <p>
            Puna Whakatau may also be interpreted as a proficient Māori nurse,
            who is expected to have in-depth knowledge of dual-competency-based
            nursing practice and perceive situations as a whole. Comprehension
            of significant concepts is based on previous experience. Māori
            nurses at this level should be able to demonstrate the ability to
            recognise situational changes that require unplanned or
            unanticipated interventions. They should also be able to provide
            leadership within a health care team, to assist in formulating
            integrated approaches to care.
          </p>
          <p>They will:</p>
          <ul>
            <li>
              demonstrate competency in a wide range of clinical skills and
              Pukenga Māori motuhake;
            </li>
            <li>
              apply knowledge of theory and practice to endorse and challenge
              appropriateness of care;
            </li>
            <li>
              realise the impact that life events and health status changes have
              on tangata whaiora/whānau, and are able to offer support and
              guidance based on their knowledge and experience of practice;
            </li>
            <li>
              work from a variety of perspectives, having learnt from experience
              what typical events to expect in a given situation and how plans
              need to be modified in response to these events;
            </li>
            <li>
              communicate clearly and effectively in rapidly changing
              situations, and recognising the important aspects of a situation
              and deal appropriately; and
            </li>
            <li>
              be recognised as role models and actively participate as
              preceptors for less experienced registered Māori nurses.
            </li>
          </ul>
          <h3>Puna Rahi - Expert Nurse</h3>
          <p>
            Puna Rahi may also be interpreted as the expert Māori nurse, who is
            expected to have intuition and skill arising from a broad dual
            competency-based knowledge base, grounded in experience and enhanced
            by ongoing education and, clinical and cultural professional
            development. Delivery of practice is guided by a flexible,
            innovative and confident approach to tangata whaiora/whānau care.
            The expert Māori nurse operates from a deep understanding of a
            holistic situation. They demonstrate collaborative practice with
            other members of the health care team to co-ordinate resources and
            maximise advocacy for tangata whaiora/whānau. Establishment of a
            trusting relationship with tangata whaiora/whānau is founded in a
            philosophy of partnership and tino rangatiratanga. The Māori nurse
            is recognised within the health care team as a leader and educator,
            and works proactively to promote Māori models of practice as
            recognised, utilised and valued by the discipline of nursing and
            services.
          </p>
          <p>They will:</p>
          <ul>
            <li>assess and plan care in a holistic manner;</li>
            <li>
              treat the actual concerns and needs of tangata whaiora and whānau
              as of utmost importance, even if this means planning and
              negotiating for a change in the plan of care;
            </li>
            <li>
              be extremely competent in a wide range of dual competency based
              skills and techniques;
            </li>
            <li>
              use in depth clinically and culturally appropriate assessment
              processes;
            </li>
            <li>
              consistently act as pro-active role models for Māori nurses;
            </li>
            <li>accept responsibility for mentorship of Māori colleagues;</li>
            <li>
              have an intuitive grasp of the situation and are able to rapidly
              and accurately identify relevant factors to initiate appropriate
              responses;
            </li>
            <li>
              have a clear vision of how situations could potentially develop;
              and
            </li>
            <li>
              actively participate in the decision-making processes for planning
              and managing resources.
            </li>
          </ul>
        </Dialog>
      </Fragment>
    );
  }
}

export default injectSheet(classStyles)(HowToChoosePuna);
