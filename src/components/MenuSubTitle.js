import React from 'react'
import injectSheet from 'react-jss';

const classStyles = (theme: JssTheme) => ({
  menuSubTitle: {
  	color: theme.palette.primary3Color,
  	padding: '0 16px'
  }
});

class MenuSubTitle extends React.Component {
	render() {
    const { classes } = this.props;
    return (
      <p className={classes.menuSubTitle}>{ this.props.children }</p>
    );
  }
}

export default injectSheet(classStyles)(MenuSubTitle);
