// @flow
import React, { Component } from "react";
import { connect } from "react-redux";
import injectSheet from "react-jss";
import DatePicker from "material-ui/DatePicker";
import RaisedButton from "material-ui/RaisedButton";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import {
  createEvidence,
  loadEvidences,
  setStatusDialog
} from "../actions/actionCreators";
import Dialog from "material-ui/Dialog";
import FlatButton from "material-ui/FlatButton";
import { List, ListItem } from "material-ui/List";
import Divider from "material-ui/Divider";
import { Card, CardHeader } from "material-ui/Card";
import Heading from "./Heading";
import { evidenceFilter } from "../helpers/evidenceHelper";
import moment from "moment";

const classStyles = (theme: JssTheme) => ({});

export class ProfessionalDevelopment extends Component {
  constructor(props) {
    super(props);
    this._initState = {
      value: 1,
      openErrorDialog: false,
      errors: {},
      open: false,
      date: new Date(),
      courseName: "",
      deliveredBy: "",
      hoursAttended: 0,
      learningGained: ""
    };
    this.state = this._initState;

    this.handleClose = this.handleClose.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.resetState = this.resetState.bind(this);
  }

  resetState() {
    this.setState(this._initState);
  }

  componentDidMount() {
    const { setStatusDialog, loadEvidences, userId, apiToken } = this.props;
    loadEvidences({ user_id: userId, api_token: apiToken });
    setStatusDialog("Loading Professional Development");
  }

  handleChange(event, date) {
    if (event === null) {
      this.setState({
        date: date
      });
    } else {
      this.setState({
        [event.target.name]: event.target.value
      });
    }
  }

  handleErrors(error) {
    this.setState({
      openErrorDialog: true,
      errors: error
    });
  }

  handleClose() {
    this.setState({
      open: false,
      openErrorDialog: false
    });
  }

  errorDialog() {
    const actions = [
      <FlatButton label="Close" primary={true} onClick={this.handleClose} />
    ];
    return (
      <Dialog modal={false} actions={actions} open={this.state.openErrorDialog}>
        <p>You missed some required fields</p>
      </Dialog>
    );
  }

  render() {
    const {
      statusDialog,
      statusDialogMessage,
      evidences,
      createEvidence,
      apiToken,
      userId
    } = this.props;

    const filteredEvidences = evidenceFilter(evidences, 1);

    return (
      <div>
        <Dialog modal={false} open={statusDialog}>
          <p>{statusDialogMessage}</p>
        </Dialog>
        {this.errorDialog()}
        <Heading>Professional Development</Heading>
        <Card style={{ marginBottom: "30px" }}>
          <CardHeader title="Your Professional Development Records" />
          <Divider />
          <List>
            {filteredEvidences.length > 0 ? (
              filteredEvidences.map(function(evidence, index) {
                return (
                  <ListItem key={index}>
                    {evidence.name}, by {evidence.facilitator}, on{" "}
                    {evidence.date}, for {evidence.hours} hours -{" "}
                    {evidence.content}
                  </ListItem>
                );
              })
            ) : (
              <ListItem disabled={true}>
                You don't yet have any records please add one below.
              </ListItem>
            )}
          </List>
        </Card>

        <Card>
          <CardHeader title="Add a Professional Development Record" />
          <Divider />
          <List>
            <ListItem disabled={true}>
              <ValidatorForm
                ref="professionalDevelopmentRecord"
                onError={errors => this.handleErrors(errors)}
                onSubmit={event => {
                  createEvidence({
                    user_id: userId,
                    api_token: apiToken,
                    date: moment(this.state.date).format("YYYY-MM-DD"),
                    courseName: this.state.courseName,
                    deliveredBy: this.state.deliveredBy,
                    hoursAttended: this.state.hoursAttended,
                    learningGained: this.state.learningGained,
                    evidenceType: 1
                  });
                  this.resetState();
                }}
              >
                <DatePicker
                  name="date"
                  hintText="Date Attended"
                  floatingLabelText="Date of Course"
                  onChange={this.handleChange}
                  value={this.state.date}
                />
                <TextValidator
                  name="courseName"
                  floatingLabelText="Course Name"
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                  onChange={this.handleChange}
                  value={this.state.courseName}
                />
                <br />
                <TextValidator
                  name="deliveredBy"
                  floatingLabelText="Delivered By"
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                  onChange={this.handleChange}
                  value={this.state.deliveredBy}
                />
                <br />
                <TextValidator
                  name="hoursAttended"
                  floatingLabelText="Hours Attended"
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                  onChange={this.handleChange}
                  value={this.state.hoursAttended}
                />
                <br />
                <TextValidator
                  name="learningGained"
                  floatingLabelText="Learning Gained"
                  multiLine={true}
                  fullWidth={true}
                  rows={5}
                  rowsMax={5}
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                  onChange={this.handleChange}
                  value={this.state.learningGained}
                />
                <RaisedButton label="Save" primary={true} type="submit" />
              </ValidatorForm>
            </ListItem>
          </List>
        </Card>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    statusDialog: state.statusDialog.display,
    statusDialogMessage: state.statusDialog.message,
    apiToken: state.login.loginStatus.response.data.api_token,
    userId: state.login.loginStatus.response.data.id,
    evidences: state.evidence.evidences
  };
};
const mapDispatchToProps = dispatch => ({
  loadEvidences(userId) {
    dispatch(loadEvidences(userId));
  },
  setStatusDialog(message) {
    dispatch(setStatusDialog(true, message));
  },
  createEvidence(content) {
    dispatch(createEvidence(content));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectSheet(classStyles)(ProfessionalDevelopment));
