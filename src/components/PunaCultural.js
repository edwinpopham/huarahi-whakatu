import React, { Component } from "react";
import injectSheet from "react-jss";
import SelectField from "material-ui/SelectField";
import MenuItem from "material-ui/MenuItem";

const classStyles = (theme: JssTheme) => ({
  punaLink: {
    padding: "5px",
    color: theme.palette.primary1Color,
    cursor: "pointer",
    margin: "20px 0",
    display: "block"
  },
  punaWrapper: {
    display: "flex",
    flexDirection: "column"
  }
});

class PunaCultural extends Component {
  constructor(props) {
    super(props);
    this.state = {
      disabled: false,
      value: this.props.competencyData.puna_id
    };
  }

  handleChange = (event, key, payload) => {
    const { competencyData, handleChange } = this.props;
    this.setState({
      value: payload
    });
    handleChange(payload, competencyData.id);
  };

  render() {
    const { classes, handleChange, competencyData } = this.props;

    return (
      <div className={classes.punaWrapper}>
        <SelectField
          floatingLabelText="Puna"
          value={this.state.value || 1}
          onChange={(event, key, payload) =>
            this.handleChange(event, key, payload)
          }
          disabled={this.state.disabled}
        >
          <MenuItem value={1} primaryText="Whakato" />
          <MenuItem value={2} primaryText="Whakatau" />
          <MenuItem value={3} primaryText="Rahi" />
        </SelectField>
      </div>
    );
  }
}

export default injectSheet(classStyles)(PunaCultural);
