// @flow
import React, { Component } from "react";
import injectSheet from "react-jss";
import RaisedButton from "material-ui/RaisedButton";
import { connect } from "react-redux";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import { requestPeerReviewer } from "../actions/actionCreators";
import MediaQuery from "react-responsive";
import GooglePlaceAutocomplete from "material-ui-places";
import FlatButton from "material-ui/FlatButton";
import Dialog from "material-ui/Dialog";
import Alert from "./Alert";
import {
  extractFirstname,
  extractMiddlenames,
  extractLastname
} from "../helpers/nameExtractions";
import {
  extractAddress,
  extractSuburb,
  extractCity
} from "../helpers/addressExtractions";
import { LARGE_SIZE } from "../constants/breakPoints";

const classStyles = (theme: JssTheme) => ({
  notification: {
    color: "red",
    fontWeight: "bold"
  }
});

export class Reviewer extends Component {
  constructor(props) {
    super(props);
    const { peer } = this.props;
    this.state = {
      disabled: peer ? true : false,
      fullName: peer
        ? peer.peer_details.first_name + " " + peer.peer_details.surname
        : "",
      jobTitle: "",
      regNumber: "",
      emailAddress: "",
      mobileNumber: "",
      addressField: "",
      suburbField: "",
      cityField: "",
      homeAddress: "",
      open: false,
      openErrorDialog: false,
      dialogName: "",
      errors: {}
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleDisabled = this.handleDisabled.bind(this);
  }

  handleChange(event, index, value, name) {
    this.setState({
      [name]: event.target.value
    });
  }

  handleErrors(error) {
    this.setState({
      openErrorDialog: true,
      errors: error
    });
  }

  handleClose() {
    this.setState({
      open: false,
      openErrorDialog: false
    });
  }

  handleDisabled() {
    const { peer } = this.props;
    this.setState({
      disabled: true
    });
  }

  errorDialog() {
    const actions = [
      <FlatButton label="Close" primary={true} onClick={this.handleClose} />
    ];
    return (
      <Dialog modal={false} actions={actions} open={this.state.openErrorDialog}>
        <p>You missed some required fields</p>
      </Dialog>
    );
  }

  render() {
    const {
      classes,
      userId,
      requestPeerReviewer,
      statusDialog,
      statusDialogMessage,
      pdrpId,
      apiToken
    } = this.props;
    return (
      <div>
        {this.errorDialog()}
        <Dialog modal={false} open={statusDialog}>
          <p>{statusDialogMessage}</p>
        </Dialog>
        {this.state.disabled && (
          <Alert>
            You have already chosen your reviewer, please contact us if you
            would like it changed.
          </Alert>
        )}
        <ValidatorForm
          ref="form"
          onError={errors => this.handleErrors(errors)}
          onSubmit={event => {
            this.handleDisabled();
            requestPeerReviewer({
              first_name: extractFirstname(this.state.fullName),
              middle_names: extractMiddlenames(this.state.fullName),
              surname: extractLastname(this.state.fullName),
              job_title: this.state.jobTitle,
              nursing_reg_number: this.state.regNumber,
              email: this.state.emailAddress,
              mobile: this.state.mobileNumber,
              address: this.state.homeAddress
                ? extractAddress(this.state.homeAddress)
                : this.state.addressField,
              suburb: this.state.homeAddress
                ? extractSuburb(this.state.homeAddress)
                : this.state.suburbField,
              city: this.state.homeAddress
                ? extractCity(this.state.homeAddress)
                : this.state.cityField,
              country: "New Zealand",
              requesting_user: userId,
              pdrp_id: pdrpId,
              api_token: apiToken
            });
          }}
        >
          <div>
            <TextValidator
              name="fullName"
              hintText="First Middle Surname"
              floatingLabelText="Full Name"
              value={this.state.fullName}
              onChange={(event, index, value) =>
                this.handleChange(event, index, value, "fullName")
              }
              validators={["required"]}
              errorMessages={["this field is required"]}
              fullWidth={true}
              disabled={this.state.disabled}
            />
            <TextValidator
              name="jobTitle"
              hintText="Peer Reviewer Job Title"
              floatingLabelText="Job Title"
              value={this.state.jobTitle}
              onChange={(event, index, value) =>
                this.handleChange(event, index, value, "jobTitle")
              }
              validators={["required"]}
              errorMessages={["this field is required"]}
              fullWidth={true}
              disabled={this.state.disabled}
            />
            <TextValidator
              name="regNumber"
              hintText="Nursing Registration Number"
              floatingLabelText="Registration Number"
              value={this.state.regNumber}
              onChange={(event, index, value) =>
                this.handleChange(event, index, value, "regNumber")
              }
              validators={["required"]}
              errorMessages={["this field is required"]}
              fullWidth={true}
              disabled={this.state.disabled}
            />
            <TextValidator
              name="emailAddress"
              hintText="Email Address"
              floatingLabelText="Email Address"
              value={this.state.emailAddress}
              onChange={(event, index, value) =>
                this.handleChange(event, index, value, "emailAddress")
              }
              validators={["required", "isEmail"]}
              errorMessages={["this field is required", "email is not valid"]}
              fullWidth={true}
              disabled={this.state.disabled}
            />
            <TextValidator
              name="mobileNumber"
              hintText="Mobile number"
              floatingLabelText="Mobile"
              value={this.state.mobileNumber}
              onChange={(event, index, value) =>
                this.handleChange(event, index, value, "mobileNumber")
              }
              validators={["required"]}
              errorMessages={["this field is required"]}
              fullWidth={true}
              disabled={this.state.disabled}
            />
            <MediaQuery maxWidth={LARGE_SIZE - 1}>
              <TextValidator
                name="addressField"
                hintText="23 Smith Road"
                floatingLabelText="Home Street Address"
                fullWidth={true}
                value={this.state.addressField}
                onChange={(event, index, value) =>
                  this.handleChange(event, index, value, "addressField")
                }
                validators={["required"]}
                errorMessages={["this field is required"]}
                disabled={this.state.disabled}
              />
              <TextValidator
                name="suburbField"
                hintText="Henderson"
                floatingLabelText="Home Suburb"
                fullWidth={true}
                value={this.state.suburbField}
                onChange={(event, index, value) =>
                  this.handleChange(event, index, value, "suburbField")
                }
                validators={["required"]}
                errorMessages={["this field is required"]}
                disabled={this.state.disabled}
              />
              <TextValidator
                name="cityField"
                hintText="Auckland"
                floatingLabelText="Home City"
                fullWidth={true}
                value={this.state.cityField}
                onChange={(event, index, value) =>
                  this.handleChange(event, index, value, "cityField")
                }
                validators={["required"]}
                errorMessages={["this field is required"]}
                disabled={this.state.disabled}
              />
            </MediaQuery>
            <MediaQuery minWidth={LARGE_SIZE}>
              <GooglePlaceAutocomplete
                searchText={this.state.homeAddress}
                onChange={searchText => {
                  this.setState({
                    homeAddress: searchText.target.value
                  });
                }}
                onNewRequest={() => {}}
                name={"homeAddress"}
                floatingLabelText="Home Address"
                multiLine={true}
                restrictions={{ country: "nz" }}
                maxSearchResults={2}
                fullWidth={true}
                disabled={this.state.disabled}
              />
            </MediaQuery>
          </div>
          <div>
            <RaisedButton
              label="Send Invitation"
              secondary={true}
              style={{ marginRight: "10px" }}
              type="submit"
              disabled={this.state.disabled}
            />
          </div>
        </ValidatorForm>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    //registrationStatus: state.registration.registrationStatus.status
    statusDialog: state.statusDialog.display,
    statusDialogMessage: state.statusDialog.message,
    userId: state.login.loginStatus.response.data.id,
    apiToken: state.login.loginStatus.response.data.api_token
  };
};
const mapDispatchToProps = dispatch => ({
  requestPeerReviewer(details) {
    dispatch(requestPeerReviewer(details));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectSheet(classStyles)(Reviewer));
