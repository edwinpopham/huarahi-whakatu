// @flow
import React, { Component } from "react";
import injectSheet from "react-jss";
import Heading from "./Heading";
import RaisedButton from "material-ui/RaisedButton";
import TextField from "material-ui/TextField";

const classStyles = (theme: JssTheme) => ({
  textField: {
    display: 'block !important'
  }
});

export class Signup extends Component {
  render() {
    const {classes} = this.props;

    return (
      <div>
        <Heading>Signup</Heading>
        <TextField className={classes.textField} floatingLabelText="E-mail" />
        <TextField className={classes.textField} floatingLabelText="Password" type="password" />
        <RaisedButton label={"Signup"} primary={true} />
      </div>
    );
  }
}

export default injectSheet(classStyles)(Signup);
