// @flow
import React, { Component } from "react";
import injectSheet from "react-jss";
import RaisedButton from "material-ui/RaisedButton";
import { APPLY_AT_LARGE_SIZE } from "../constants/breakPoints";

const classStyles = (theme: JssTheme) => ({
  lead: {
    fontWeight: "bold"
  },
  "@global p": {
    marginTop: "0px"
  },
  homeContent: {
    [APPLY_AT_LARGE_SIZE]: {
      display: "flex"
    }
  },
  homeCopy: {
    [APPLY_AT_LARGE_SIZE]: {
      flexBasis: "60%"
    }
  },
  registerSidebar: {
    [APPLY_AT_LARGE_SIZE]: {
      marginLeft: "40px",
      flexGrow: "1"
    }
  }
});

export class Success extends Component {
  render() {
    const { classes } = this.props;

    return (
      <div className={classes.homeContent}>
        <div className={classes.homeCopy}>
          <p className={classes.lead}>
            Congratulations!, your registration was successfull.
          </p>
          <p>
            We have send your username and password to your email inbox. Contact us if you have any problems logging in.
          </p>
        </div>
        <div className={classes.registerSidebar}>
          <RaisedButton
            label="Contact us at Te Rau Matatini"
            fullWidth={true}
            secondary={true}
            href={'http://teraumatatini.com/workforce-development/focus-maori-nursing'}
          />
        </div>
      </div>
    );
  }
}

export default injectSheet(classStyles)(Success);
