// @flow
import React, { Component } from "react";
import injectSheet from "react-jss";
import Heading from "./Heading";

const classStyles = (theme: JssTheme) => ({});

export class About extends Component {
  render() {
    return (
      <div>
        <Heading>About Huarahi Whakatū</Heading>
        <p>
          The programme is coordinated by a Māori registered nurse, guided by a
          cultural and clinical governance board with access to mentors and
          Māori Assessors. The programme has been running since 2009 and in 2014
          was reaccredited by the New Zealand Nurses Council for a further five
          years. Huarahi Whakatū is the only accredited PDRP programme run
          outside of the District Health Boards that has been specifically
          developed for Māori registered nurses.
        </p>
      </div>
    );
  }
}

export default injectSheet(classStyles)(About);
