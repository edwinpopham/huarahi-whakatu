import React, { Component } from "react";
import injectSheet from "react-jss";
import { connect } from "react-redux";
import Heading from "./Heading";
import TextField from "material-ui/TextField";
import SelectField from "material-ui/SelectField";
import MenuItem from "material-ui/MenuItem";
import { setTitle, setFullName } from "../actions/registrationActionCreators";
import GooglePlaceAutocomplete from "material-ui-places";

const classStyles = (theme: JssTheme) => ({
  row: {
    display: "flex"
  },
  column: {
    flexDirection: "row",
    flexGrow: "1",
    flexBasis: "0"
  },
  subHeading: {
    fontWeight: "bold"
  },
  addressField: {}
});

class Account extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes } = this.props;

    return (
      <div>
        <Heading>Account Details</Heading>
        <div className={classes.row}>
          <div className={classes.column}>
            <p className={classes.subHeading}>Contact Details</p>
            <SelectField floatingLabelText="Title" value={"Mr."} title="title">
              <MenuItem value={0} primaryText="Please Choose" />
              <MenuItem value={"Mr."} primaryText="Mr." />
              <MenuItem value={"Ms."} primaryText="Ms." />
              <MenuItem value={"Mrs."} primaryText="Mrs." />
              <MenuItem value={"Dr."} primaryText="Dr." />
              <MenuItem value={"Prof."} primaryText="Prof." />
              <MenuItem value={"Miss."} primaryText="Miss." />
            </SelectField>
            <br />
            <TextField
              hintText="First Middle Surname"
              floatingLabelText="Full Name"
              value={"John James Doe"}
            />
            <br />
            <GooglePlaceAutocomplete
              searchText={"31 Te Mai Rd, Whangarei"}
              onChange={searchText => {
                this.setState({
                  homeAddress: searchText.target.value
                });
              }}
              onNewRequest={() => {}}
              name={"homeaddress"}
              floatingLabelText="Home Address"
              multiLine={true}
              listStyle={{ width: "500px" }}
              restrictions={{ country: "nz" }}
            />
            <br />
            <TextField
              hintText="Mobile number"
              floatingLabelText="Mobile"
              value="0410 397 457"
            />
            <br />
            <TextField
              hintText="Email Address"
              floatingLabelText="Email Address"
              value="test@testaccount.com"
            />
          </div>
          <div className={classes.column}>
            <p className={classes.subHeading}>Employer Details</p>
            <TextField
              hintText="Name of Employer"
              floatingLabelText="Name of Employer"
              value="Te Rau Matatini"
            />
            <br />
            <TextField
              hintText="Work Phone"
              floatingLabelText="Work Phone"
              value="09 437 5979"
            />
            <br />
            <GooglePlaceAutocomplete
              searchText={"13 Main Street, Whangarei"}
              onChange={searchText => {
                this.setState({
                  workAddress: searchText.target.value
                });
              }}
              onNewRequest={() => {}}
              name={"workaddress"}
              floatingLabelText="Work Address"
              multiLine={true}
              listStyle={{ width: "500px" }}
              restrictions={{ country: "nz" }}
            />
          </div>
          <div className={classes.column}>
            <p className={classes.subHeading}>Personal Details</p>
            <TextField
              hintText="Use a comma for multiple"
              floatingLabelText="Your Ethnicity"
              value="Maori, Pakeha"
            />
            <br />
            <TextField
              hintText="Use a comma for multiple"
              floatingLabelText="Your Iwi"
              value="Ngapuhi, Ngatihine"
            />
            <br />
            <TextField
              hintText="Use a comma for multiple"
              floatingLabelText="Your Hapu"
              value="Ngatihine, Ngati Mahia"
            />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    title: state.registration.title,
    fullName: state.registration.fullName
  };
};
const mapDispatchToProps = dispatch => ({
  setTitle(title) {
    dispatch(setTitle(title));
  },
  setFullName(fullName) {
    dispatch(setFullName(fullName));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(
  injectSheet(classStyles)(Account)
);
