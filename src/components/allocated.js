// @flow
import React, { Component } from "react";
import injectSheet from "react-jss";
import Heading from "./Heading";
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn
} from "material-ui/Table";
import RaisedButton from "material-ui/RaisedButton";
import AssessorSelect from "./AssessorSelect";
import ActionDoneIcon from 'material-ui/svg-icons/action/done';

const classStyles = (theme: JssTheme) => ({
  idColumn: {
    width: "50px"
  }
});

export class Allocated extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { classes } = this.props;

    return (
      <div>
        <Heading>Allocated Nurses</Heading>
        <Table>
          <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
            <TableRow>
              <TableHeaderColumn className={classes.idColumn}>
                ID
              </TableHeaderColumn>
              <TableHeaderColumn>Name</TableHeaderColumn>
              <TableHeaderColumn>Allocate</TableHeaderColumn>
              <TableHeaderColumn />
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false}>
            <TableRow>
              <TableRowColumn className={classes.idColumn}>1</TableRowColumn>
              <TableRowColumn>John Smith</TableRowColumn>
              <TableRowColumn><AssessorSelect /></TableRowColumn>
              <TableRowColumn>
                <RaisedButton label="Allocate" primary={true} />
              </TableRowColumn>
            </TableRow>
            <TableRow>
              <TableRowColumn className={classes.idColumn}>2</TableRowColumn>
              <TableRowColumn>Anne James</TableRowColumn>
              <TableRowColumn><AssessorSelect value={3} /></TableRowColumn>
              <TableRowColumn>
                <RaisedButton label="Allocated" disabled={true} icon={<ActionDoneIcon />} />
              </TableRowColumn>
            </TableRow>
            <TableRow>
              <TableRowColumn className={classes.idColumn}>3</TableRowColumn>
              <TableRowColumn>Anita Mack</TableRowColumn>
              <TableRowColumn><AssessorSelect /></TableRowColumn>
              <TableRowColumn>
                <RaisedButton label="Allocate" primary={true} />
              </TableRowColumn>
            </TableRow>
          </TableBody>
        </Table>
      </div>
    );
  }
}

export default injectSheet(classStyles)(Allocated);
