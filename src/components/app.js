import About from "./about";
import Account from "./account";
import Allocated from "./allocated";
import AdminAllUsers from "./AdminAllUsers";
import AdminReadyAssign from "./AdminReadyAssign";
import AdminAssigned from "./AdminAssigned";
import AppBar from "material-ui/AppBar";
import Content from "./Content";
import Dashboard from "./dashboard";
import Drawer from "material-ui/Drawer";
import Footer from "./Footer";
import Home from "./home";
import injectSheet from "react-jss";
import Login from "./login";
import Logout from "./logout";
import MenuItem from "material-ui/MenuItem";
import PukengaHaumanu from "./pukengaHaumanu";
import PukengaMaoriMotuhake from "./pukengaMaoriMotuhake";
import React, { Component } from "react";
import Registration from "./registration";
import Reports from "./reports";
import Signup from "./Signup";
import Success from "./Success";
import Submitted from "./submitted";
import Supervision from "./Supervision";
import ProfessionalDevelopment from "./ProfessionalDevelopment";
import Practice from "./Practice";
import Evidence from "./Evidence";
import { Route, Link, Switch, withRouter } from "react-router-dom";
import { connect } from "react-redux";

const classStyles = (theme: JssTheme) => ({
  appBar: {
    position: "fixed !important",
    top: "0"
  },
  title: {
    cursor: "pointer"
  }
});

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { open: false };
    this.handleToggle = this.handleToggle.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  handleToggle() {
    this.setState({ open: !this.state.open });
  }

  handleClose() {
    this.setState({ open: false });
  }

  render() {
    const {
      history,
      classes,
      apiToken,
      pdrpExistStatus = [],
      userRole
    } = this.props;
    // const loginButton = (
    //   <FlatButton
    //     containerElement={<Link to="/login" />}
    //     icon={<AccountCircle />}
    //     label="Login"
    //   />
    // );

    const ScrollToTop = () => {
      window.scrollTo(0, 0);
      return null;
    };

    return (
      <div>
        <AppBar
          title={<span className={classes.title}>Huarahi Whakatū</span>}
          onLeftIconButtonClick={this.handleToggle}
          onTitleClick={() => {
            history.push("/");
          }}
          className={classes.appBar}
        />
        <Drawer
          containerClassName={classes.drawer}
          open={this.state.open}
          docked={false}
        >
          <MenuItem
            onClick={this.handleClose}
            containerElement={<Link to="/" />}
          >
            Home
          </MenuItem>
          {apiToken === undefined &&
            pdrpExistStatus.length > 0 && (
              <MenuItem
                onClick={this.handleClose}
                containerElement={<Link to="/registration" />}
              >
                Registration
              </MenuItem>
            )}
          {apiToken === undefined && (
            <MenuItem
              onClick={this.handleClose}
              containerElement={<Link to="/login" />}
            >
              Login
            </MenuItem>
          )}
          {apiToken !== undefined && (
            <MenuItem
              onClick={this.handleClose}
              containerElement={<Link to="/dashboard" />}
            >
              Dashboard
            </MenuItem>
          )}
          {apiToken !== undefined &&
            pdrpExistStatus.length > 0 && (
              <MenuItem
                onClick={this.handleClose}
                containerElement={<Link to="/pukenga-haumanu" />}
              >
                Pukenga Haumanu
              </MenuItem>
            )}
          {apiToken !== undefined &&
            pdrpExistStatus.length > 0 && (
              <MenuItem
                onClick={this.handleClose}
                containerElement={<Link to="/pukenga-maori-motuhake" />}
              >
                Pukenga Māori Motuhake
              </MenuItem>
            )}
          {apiToken !== undefined &&
            pdrpExistStatus.length > 0 && (
              <MenuItem
                onClick={this.handleClose}
                containerElement={<Link to="/supervision-hours" />}
              >
                Supervision Hours
              </MenuItem>
            )}
          {apiToken !== undefined &&
            pdrpExistStatus.length > 0 && (
              <MenuItem
                onClick={this.handleClose}
                containerElement={<Link to="/professional-development-hours" />}
              >
                Professional Development
              </MenuItem>
            )}
          {apiToken !== undefined &&
            pdrpExistStatus.length > 0 && (
              <MenuItem
                onClick={this.handleClose}
                containerElement={<Link to="/practice-hours" />}
              >
                Practice Hours
              </MenuItem>
            )}
          {apiToken !== undefined &&
            pdrpExistStatus.length > 0 && (
              <MenuItem
                onClick={this.handleClose}
                containerElement={<Link to="/additional-evidence" />}
              >
                Additional Evidence
              </MenuItem>
            )}
          {userRole === 4 &&
            pdrpExistStatus.length > 0 && (
              <MenuItem
                onClick={this.handleClose}
                containerElement={<Link to="/all-users" />}
              >
                Admin Area
              </MenuItem>
            )}
          {apiToken !== undefined && (
            <MenuItem
              onClick={this.handleClose}
              containerElement={<Link to="/logout" />}
            >
              Logout
            </MenuItem>
          )}
        </Drawer>
        <Content>
          <Route component={ScrollToTop} />
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/about-us" component={About} />
            <Route exact path="/registration" component={Registration} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/logout" component={Logout} />
            <Route exact path="/signup" component={Signup} />
            <Route exact path="/submitted" component={Submitted} />
            <Route exact path="/allocated" component={Allocated} />
            <Route exact path="/reports" component={Reports} />
            <Route exact path="/account" component={Account} />
            <Route exact path="/pukenga-haumanu" component={PukengaHaumanu} />
            <Route exact path="/supervision-hours" component={Supervision} />
            <Route
              exact
              path="/professional-development-hours"
              component={ProfessionalDevelopment}
            />
            <Route exact path="/practice-hours" component={Practice} />
            <Route exact path="/additional-evidence" component={Evidence} />
            <Route exact path="/dashboard" component={Dashboard} />
            <Route
              exact
              path="/pukenga-maori-motuhake"
              component={PukengaMaoriMotuhake}
            />
            <Route exact path="/success" component={Success} />
            <Route path="/pukenga-haumanu/:id" component={PukengaHaumanu} />
            <Route
              path="/pukenga-maori-motuhake/:id"
              component={PukengaMaoriMotuhake}
            />
            <Route path="/all-users" component={AdminAllUsers} />
            <Route path="/admin-ready" component={AdminReadyAssign} />
            <Route path="/admin-assigned" component={AdminAssigned} />
          </Switch>
        </Content>
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    login: state.login,
    userRole: state.login.loginStatus.response.data.roles,
    apiToken: state.login.loginStatus.response
      ? state.login.loginStatus.response.data.api_token
      : state.login.loginStatus.response,
    pdrpExistStatus: state.pdrp.pdrpExist.status
    //registrationStatus: state.registration.registrationStatus.status
  };
};
const mapDispatchToProps = dispatch => ({
  // requestRegistration(registrationDetails) {
  //   dispatch(requestRegistration(registrationDetails));
  // }
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(injectSheet(classStyles)(App))
);
