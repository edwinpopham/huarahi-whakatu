// @flow
import React, { Component, Fragment } from "react";
import injectSheet from "react-jss";
import Heading from "./Heading";
import RaisedButton from "material-ui/RaisedButton";
import FlatButton from "material-ui/FlatButton";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import Dialog from "material-ui/Dialog";
import { List, ListItem } from "material-ui/List";
import Divider from "material-ui/Divider";
import Done from "material-ui/svg-icons/action/done";
import ToDo from "material-ui/svg-icons/action/info";
import WarningIcon from "material-ui/svg-icons/alert/warning";
import VideoIcon from "material-ui/svg-icons/av/movie";
import Menu from "material-ui/Menu";
import MenuItem from "material-ui/MenuItem";
import { requestPdrpStart, requestPdrpExist } from "../actions/actionCreators";

const classStyles = (theme: JssTheme) => ({
  menuItem: {
    lineHeight: "30px"
  },
  menuItemLink: {
    color: theme.palette.primary1Color,
    textDecoration: "none"
  },
  maxWidthText: {
    maxWidth: "400px",
    lineHeight: "24px"
  }
});

export class Dashboard extends Component {
  componentDidMount() {
    const { userId, apiToken, requestPdrpExist } = this.props;

    requestPdrpExist({ user_id: userId, api_token: apiToken });
  }

  statusDialog(message) {
    const { statusDialog = false } = this.props;

    return (
      <Dialog modal={false} open={statusDialog}>
        <p>{message}</p>
      </Dialog>
    );
  }

  pdrpStartStatusDialog() {
    const { pdrpStartDialogOpen = false } = this.props;

    return (
      <Dialog modal={false} open={pdrpStartDialogOpen}>
        <p>Starting your PDRP...</p>
      </Dialog>
    );
  }

  render() {
    const {
      apiToken,
      name,
      userId,
      classes,
      requestPdrpStart,
      pdrpsToReview = [],
      pdrpsToAssess = [],
      pdrpExistStatus = [],
      haumanuPdrpPeer,
      motuhakePdrpPeer
    } = this.props;

    return (
      <div>
        {this.pdrpStartStatusDialog()}
        {this.statusDialog("Loading Dashboard...")}
        {apiToken !== undefined && (
          <div>
            <Heading>Dashboard</Heading>
            <p>Hi {name}, welcome back!</p>
          </div>
        )}
        {apiToken === undefined && (
          <p>Sorry you must be logged in to view this page.</p>
        )}
        {pdrpExistStatus.length > 0 && <Divider />}
        {pdrpsToReview.length > 0 && (
          <div>
            <h2>PDRP's to Review</h2>
            <List>
              {pdrpsToReview.map(function(pdrpToReview, index) {
                return (
                  <ListItem
                    key={index}
                    containerElement={
                      <Link
                        to={
                          pdrpToReview.type_id === 1
                            ? "/pukenga-haumanu/" + pdrpToReview.user_id
                            : "/pukenga-maori-motuhake/" + pdrpToReview.user_id
                        }
                      />
                    }
                  >
                    {pdrpToReview.nurse_first_name} {pdrpToReview.nurse_surname}
                    ,{" "}
                    {pdrpToReview.type_id === 1
                      ? "Pukenga Haumanu"
                      : "Pukenga Māori Motuhake"}
                  </ListItem>
                );
              })}
            </List>
          </div>
        )}
        {pdrpsToAssess.length > 0 && (
          <div>
            <h2>PDRP's to Asess</h2>
            <List>
              {pdrpsToAssess.map(function(pdrpToAssess, index) {
                return (
                  <ListItem
                    key={index}
                    containerElement={
                      <Link
                        to={
                          pdrpToAssess.type_id === 1
                            ? "/pukenga-haumanu/" + pdrpToAssess.user_id
                            : "/pukenga-maori-motuhake/" + pdrpToAssess.user_id
                        }
                      />
                    }
                  >
                    {pdrpToAssess.nurse_first_name} {pdrpToAssess.nurse_surname}
                    ,{" "}
                    {pdrpToAssess.type_id === 1
                      ? "Pukenga Haumanu"
                      : "Pukenga Māori Motuhake"}
                  </ListItem>
                );
              })}
            </List>
          </div>
        )}
        <Divider />
        <h2>Your Personal PDRP Progress</h2>
        {pdrpExistStatus.length === 0 && (
          <Fragment>
            <p className="test-startPdrpWarning">
              <WarningIcon /> If you are just doing a peer review or assessment
              you don't need to click this button. Click on the review or
              assessment above.
            </p>
            <RaisedButton
              label="Start your PDRP"
              secondary={true}
              onClick={event =>
                requestPdrpStart({ user_id: userId, api_token: apiToken })
              }
            />
          </Fragment>
        )}
        {pdrpExistStatus.length > 0 && (
          <div>
            <Divider />
            <p>
              <WarningIcon /> Please read the{" "}
              <a
                href="/nurses_tool-kit_handbook.pdf"
                className={classes.menuItemLink}
                target="blank"
              >
                Nurses Toolkit
              </a>{" "}
              and agree to the{" "}
              <a
                href="/declaration.pdf"
                className={classes.menuItemLink}
                target="blank"
              >
                declaration
              </a>{" "}
              before proceeding.
            </p>
            <p>
              If you have any questions about the program, please email Kim at{" "}
              <a
                className={classes.menuItemLink}
                href="mailto:kim.wi@terauora.com"
              >
                kim.wi@terauora.com
              </a>
              .
              <br />
              Watch a{" "}
              <a
                href="https://youtu.be/o6Qy4A51vNI"
                className={classes.menuItemLink}
                target="blank"
              >
                screen recording video
              </a>{" "}
              that shows you how to navigate and enter your PDRP online.
            </p>
            <p>When you are ready fill out your PDRP online:</p>
            <p>
              <FlatButton
                href="/pukenga-haumanu"
                label="Pukenga Haumanu"
                primary={true}
              />
              <br />
              <FlatButton
                href="/pukenga-maori-motuhake"
                label="Pukenga Māori Motuhake"
                primary={true}
              />
              <br />
              <FlatButton
                href="/supervision-hours"
                label="Supervision Hours"
                primary={true}
              />
              <br />
              <FlatButton
                href="/professional-development-hours"
                label="Professional Development"
                primary={true}
              />
              <br />
              <FlatButton
                href="/practice-hours"
                label="Practice Hours"
                primary={true}
              />
              <br />
              <FlatButton
                href="/additional-evidence"
                label="Additional Evidence"
                primary={true}
              />
            </p>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    login: state.login,
    userId: state.login.loginStatus.response.data.id,
    apiToken: state.login.loginStatus.response.data.api_token,
    name: state.login.loginStatus.response.data.name,
    pdrpStartDialogOpen: state.pdrp.pdrpStartStatus.dialogOpen,
    pdrpExistStatus: state.pdrp.pdrpExist.status,
    haumanuPdrpPeer: state.pdrp.pdrpPeers.response
      ? state.pdrp.pdrpPeers.response[1]
      : state.pdrp.pdrpPeers.response,
    motuhakePdrpPeer: state.pdrp.pdrpPeers.response
      ? state.pdrp.pdrpPeers.response[2]
      : state.pdrp.pdrpPeers.response,
    statusDialog: state.statusDialog.display,
    pdrpsToReview: state.pdrp.pdrpsToReview
      ? state.pdrp.pdrpsToReview.response
      : state.pdrp.pdrpsToReview,
    pdrpsToAssess: state.pdrp.pdrpsToAssess
      ? state.pdrp.pdrpsToAssess.response
      : state.pdrp.pdrpsToAssess
    //registrationStatus: state.registration.registrationStatus.status
  };
};
const mapDispatchToProps = dispatch => ({
  requestPdrpStart(userId) {
    dispatch(requestPdrpStart(userId));
  },
  requestPdrpExist(userId) {
    dispatch(requestPdrpExist(userId));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectSheet(classStyles)(Dashboard));
