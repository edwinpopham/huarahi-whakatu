// @flow
import React, { Component } from "react";
import injectSheet from "react-jss";
import { connect } from "react-redux";
import RaisedButton from "material-ui/RaisedButton";
import { APPLY_AT_LARGE_SIZE } from "../constants/breakPoints";
import { Link } from "react-router-dom";

const classStyles = (theme: JssTheme) => ({
  lead: {
    fontWeight: "bold"
  },
  "@global p": {
    marginTop: "0px"
  },
  homeContent: {
    [APPLY_AT_LARGE_SIZE]: {
      display: "flex"
    }
  },
  homeCopy: {
    [APPLY_AT_LARGE_SIZE]: {
      flexBasis: "60%"
    }
  },
  registerSidebar: {
    [APPLY_AT_LARGE_SIZE]: {
      marginLeft: "40px",
      flexGrow: "1"
    }
  }
});

export class Home extends Component {
  render() {
    const { classes, loginStatus } = this.props;

    return (
      <div className={classes.homeContent}>
        <div className={classes.homeCopy}>
          <h1>Sorry we are currently doing some maintenance on the site. We will be back online on Monday. Kind Regards</h1>
          <p className={classes.lead}>
            Huarahi Whakatū is an on-line Nursing Council Accredited
            Professional Development and Recognition Programme (PDRP)
            specifically tailored by, and for, Māori Registered Nurses.
          </p>
          <p>
            The programme is co-ordinated by a Māori registered nurse, guided by
            a cultural and clinical governance board with access to mentors and
            Māori Assessors. The programme has been running since 2009, and in
            2014 was reaccredited by the New Zealand Nursing Council for a
            further five years. Huarahi Whakatū is accepted within District
            Health Boards, Government and non-Government organisations and is
            the only accredited PDRP programme run outside of the District
            Health Boards.
          </p>
          <p>
            The Huarahi Whakatū PDRP promotes the philosophy of ‘dual
            competency', that is clinical and cultural competencies. Clinical
            competencies are drawn from the Nursing Council of New Zealand,
            whereas cultural competencies are informed by Te Ao Māori.
          </p>
          <p>
            A range of Māori Registered Nurses throughout the country are
            engaged with the Huarahi Whakatū professional development programme,
            with Māori nurses successfully completing the programme across
            sectors.
          </p>
          <p>
            District Health Boards, Maori Providers, Government and
            Non-Government organisations are allies to ensure Māori Registered
            Nurses receive professional development hours in order to complete
            their portfolio as per New Zealand Nursing Council requirements.
            Registered nurses that complete Puna Whakatau and Puna Rahi are
            entitled to remuneration payments, as per New Zealand Nursing
            Organisation or Public Service Association. Huarahi Whakatu have
            four levels of competencies – Puna Whakato (Competent), Puna
            Whakatau (Proficient), Puna Rahi (Expert), and Puna Rangatira
            (Senior Nurses and Nurse Educators).
          </p>
          <p>Huarahi Whakatu online E Portfolios.</p>
          <ul>
            <li>
              First and only Indigenous Dual Competency Nursing programme in
              Aotearoa.
            </li>
            <li>Endorsed by New Zealand Nursing Council.</li>
            <li>Huarahi Whakatū now has easy online access</li>
            <li>Ability to log on from anywhere.</li>
            <li>Easy to follow instructions, oral and written.</li>
            <li>Access to programme co-ordinator for support.</li>
            <li>Recognise and reward cultural and clinical excellence.</li>
            <li>Strengthen best practice standards of Māori nursing care.</li>
            <li>
              Provide a framework for professional growth and development.
            </li>
            <li>
              Supported and directed by Te Rōpu Korowai for Cultural and
              Clinical advice.
            </li>
            <li>
              Te Rōpu Korowai is to wrap around the Kaupapa of the programme
              keeping and guiding the Tikanga to ensure safety of practice.
            </li>
            <li>
              Oversee the cultural and clinical aspects required to ensure the
              Tapu and Mana of Huarahi Whakatū stay’s Pono to the Kaupapa.
            </li>
          </ul>
        </div>
        {loginStatus !== "success" && (
          <div className={classes.registerSidebar}>
            <RaisedButton
              label="Register for Huarahi Whakatū"
              fullWidth={true}
              secondary={true}
              containerElement={<Link to="/registration" />}
            />
            <RaisedButton
              label="Login (if already Registered)"
              fullWidth={true}
              primary={true}
              style={{ marginTop: "10px" }}
              containerElement={<Link to="/login" />}
            />
          </div>
        )}
        {loginStatus === "success" && (
          <div className={classes.registerSidebar}>
            <RaisedButton
              label="Dashboard"
              fullWidth={true}
              secondary={true}
              containerElement={<Link to="/dashboard" />}
            />
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    loginStatus: state.login.loginStatus.status
  };
};
const mapDispatchToProps = dispatch => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectSheet(classStyles)(Home));
