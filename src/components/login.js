// @flow
import React, { Component } from "react";
import { connect } from "react-redux";
import injectSheet from "react-jss";
import Heading from "./Heading";
import ErrorContainer from "./ErrorContainer";
import RaisedButton from "material-ui/RaisedButton";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import Dialog from "material-ui/Dialog";
import FlatButton from "material-ui/FlatButton";
import {
  requestLogin,
  setStatusDialog,
  setErrorMessage,
  requestReset
} from "../actions/actionCreators";
import { Redirect } from "react-router";

const classStyles = (theme: JssTheme) => ({
  textField: {}
});

export class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      errors: {},
      openErrorDialog: false,
      emailAddress: "",
      password: "",
      passwordReset: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleDisplay = this.handleDisplay.bind(this);
  }

  componentDidMount() {
    const { setErrorMessage } = this.props;
    setErrorMessage(false);
  }

  handleChange(event, index, value, name) {
    switch (name) {
      default:
        this.setState({
          [name]: event.target.value
        });
        break;
    }
  }

  handleDisplay() {
    this.setState({
      passwordReset: !this.state.passwordReset
    });
  }

  handleClose() {
    this.setState({
      open: false,
      openErrorDialog: false
    });
  }

  handleErrors(error) {
    this.setState({
      openErrorDialog: true,
      errors: error
    });
  }

  errorDialog() {
    const actions = [
      <FlatButton label="Close" primary={true} onClick={this.handleClose} />
    ];
    return (
      <Dialog modal={false} actions={actions} open={this.state.openErrorDialog}>
        <p>You missed some required fields</p>
      </Dialog>
    );
  }

  redirectOnSuccess() {
    const { loginStatus } = this.props;
    if (loginStatus === "success") {
      return <Redirect to="/dashboard" push={true} />;
    }
  }

  render() {
    const {
      classes,
      requestLogin,
      statusDialog,
      statusDialogMessage,
      requestReset,
      requestReceived
    } = this.props;

    return (
      <div>
        <ErrorContainer />
        <Dialog modal={false} open={statusDialog}>
          <p>{statusDialogMessage}</p>
        </Dialog>
        {!this.state.passwordReset && (
          <ValidatorForm
            ref="form"
            onError={errors => this.handleErrors(errors)}
            onSubmit={event =>
              requestLogin({
                email: this.state.emailAddress,
                password: this.state.password
              })
            }
          >
            {this.errorDialog()}
            {this.redirectOnSuccess()}
            <Heading>Login</Heading>
            <TextValidator
              name="emailAddress"
              hintText="Email Address"
              floatingLabelText="Email Address"
              validators={["required", "isEmail"]}
              errorMessages={["this field is required", "email is not valid"]}
              fullWidth={true}
              className={classes.textField}
              value={this.state.emailAddress}
              onChange={(event, index, value) =>
                this.handleChange(event, index, value, "emailAddress")
              }
            />
            <TextValidator
              name="password"
              floatingLabelText="Password"
              validators={["required"]}
              errorMessages={["this field is required", "email is not valid"]}
              fullWidth={true}
              type="password"
              className={classes.textField}
              value={this.state.password}
              onChange={(event, index, value) =>
                this.handleChange(event, index, value, "password")
              }
            />
            <RaisedButton label={"Login"} primary={true} type="submit" />{" "}
            <RaisedButton
              label={"Forgot Password?"}
              primary={false}
              onClick={this.handleDisplay}
            />
          </ValidatorForm>
        )}
        {this.state.passwordReset && (
          <ValidatorForm
            ref="resetform"
            onError={errors => this.handleErrors(errors)}
            onSubmit={event =>
              requestReset({
                email: this.state.emailAddress
              })
            }
          >
            {this.errorDialog()}
            {this.redirectOnSuccess()}
            <Heading>Password Reset</Heading>
            {!requestReceived && (
              <div>
                <TextValidator
                  name="emailAddress"
                  hintText="Email Address"
                  floatingLabelText="Email Address"
                  validators={["required", "isEmail"]}
                  errorMessages={[
                    "this field is required",
                    "email is not valid"
                  ]}
                  fullWidth={true}
                  className={classes.textField}
                  value={this.state.emailAddress}
                  onChange={(event, index, value) =>
                    this.handleChange(event, index, value, "emailAddress")
                  }
                />
                <RaisedButton
                  label={"Reset Password"}
                  primary={true}
                  type="submit"
                />{" "}
                <RaisedButton
                  label={"Login"}
                  primary={false}
                  onClick={this.handleDisplay}
                />
              </div>
            )}
            {requestReceived && (
              <div>
                <p>
                  A password reset has been processed. If a valid email exists
                  we will send you an email with a new password.
                </p>
                <RaisedButton
                  label={"Login"}
                  primary={false}
                  onClick={this.handleDisplay}
                />
              </div>
            )}
          </ValidatorForm>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    loginStatusOpen: state.login.loginStatus.dialogOpen,
    loginStatus: state.login.loginStatus.status,
    apiToken: state.login.loginStatus.response
      ? state.login.loginStatus.response.data.api_token
      : state.login.loginStatus.response,
    statusDialog: state.statusDialog.display,
    statusDialogMessage: state.statusDialog.message,
    requestReceived: state.errorMessage.resetRequested
  };
};
const mapDispatchToProps = dispatch => ({
  requestLogin(loginDetails) {
    dispatch(requestLogin(loginDetails));
  },
  setStatusDialog(message) {
    dispatch(setStatusDialog(true, message));
  },
  setErrorMessage(display) {
    dispatch(setErrorMessage(display));
  },
  requestReset(email) {
    dispatch(requestReset(email));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectSheet(classStyles)(Login));
