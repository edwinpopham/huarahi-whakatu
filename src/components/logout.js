// @flow
import React, { Component } from "react";
import { connect } from "react-redux";
import injectSheet from "react-jss";
import { requestLogout } from "../actions/actionCreators";

const classStyles = (theme: JssTheme) => ({
  textField: {}
});

export class Logout extends Component {
  componentDidMount() {
    const { requestLogout, apiToken } = this.props;
    requestLogout({ api_token: apiToken });
  }

  render() {
    // const { classes } = this.props;

    return (
      <div>
        <p>You are now logged out</p>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    apiToken: state.login.loginStatus.response
      ? state.login.loginStatus.response.data.api_token
      : state.login.loginStatus.response
  };
};
const mapDispatchToProps = dispatch => ({
  requestLogout(LogoutDetails) {
    dispatch(requestLogout(LogoutDetails));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectSheet(classStyles)(Logout));
