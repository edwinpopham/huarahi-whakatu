// @flow
import React, { Component } from "react";
import injectSheet from "react-jss";
import RaisedButton from "material-ui/RaisedButton";
import Heading from "./Heading";
import Competency from "./Competency";
import CompetencyGroupPukengaHaumanu from "./CompetencyGroupPukengaHaumanu";
import CardGroup from "./CardGroup";
import Reviewer from "./Reviewer";
import Dialog from "material-ui/Dialog";
import SelectField from "material-ui/SelectField";
import MenuItem from "material-ui/MenuItem";
import Alert from "./Alert";
import HowToChoosePuna from "./HowToChoosePuna";
import {
  loadCompetencies,
  setStatusDialog,
  updatePdrpPuna,
  peerPdrpComplete
} from "../actions/actionCreators";
import { connect } from "react-redux";

const classStyles = (theme: JssTheme) => ({
  punaWrapper: {
    display: "flex",
    flexDirection: "column"
  }
});

export class PukengaHaumanu extends Component {
  constructor(props) {
    super(props);
    const { punaId, userRole, pdrpPeer } = this.props;
    this.state = {
      value: punaId,
      disabled: pdrpPeer && userRole === 1 ? true : false,
      open: false
    };

    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    const {
      userId,
      apiToken,
      loadCompetencies,
      setStatusDialog,
      match
    } = this.props;
    match.params.id
      ? loadCompetencies({ user_id: match.params.id, api_token: apiToken })
      : loadCompetencies({ user_id: userId, api_token: apiToken });
    setStatusDialog("Loading competencies");
  }

  handleChange(event, index, value) {
    const {
      pakehaPdrpId,
      maoriPdrpId,
      updatePdrpPuna,
      userId,
      apiToken
    } = this.props;
    this.setState({ value });
    updatePdrpPuna({
      pakehaPdrpId: pakehaPdrpId,
      maoriPdrpId: maoriPdrpId,
      puna: value,
      user_id: userId,
      api_token: apiToken
    });
  }

  render() {
    const {
      apiToken,
      competencies,
      statusDialog,
      statusDialogMessage,
      punaId,
      pakehaPdrpId,
      match,
      pdrpPeer,
      userRole,
      peerPdrpComplete,
      classes
    } = this.props;

    return (
      <div>
        <Dialog modal={false} open={statusDialog}>
          <p>{statusDialogMessage}</p>
        </Dialog>
        <Heading>Pukenga Haumanu</Heading>
        {!competencies && (
          <p>You need to start your pdrp before filling in competencies.</p>
        )}
        {pdrpPeer &&
          userRole && (
            <Alert>
              This PDRP is currently being peer reviewed. Competency editing is
              disabled.
            </Alert>
          )}
        {competencies && (
          <div>
            <div className={classes.punaWrapper}>
              <SelectField
                floatingLabelText="Puna"
                value={this.state.value || punaId}
                onChange={this.handleChange}
                disabled={this.state.disabled}
              >
                <MenuItem value={1} primaryText="Whakato" />
                <MenuItem value={2} primaryText="Whakatau" />
                <MenuItem value={3} primaryText="Rahi" />
                <MenuItem value={4} primaryText="Rangatira" />
              </SelectField>
              <HowToChoosePuna />
            </div>
            <CompetencyGroupPukengaHaumanu
              title={"Domain 1: Professional Responsibility"}
              subtitle={
                "This domain contains competencies that relate to professional, legal and ethical responsibilities and cultural safety. These include being able to demonstrate knowledge and judgment and being accountable for own actions and decisions, while promoting an environment that maximises client’ safety, independence, quality of life and health."
              }
            >
              <Competency
                description={
                  "Accepts responsibility for ensuring that his/her nursing practice and conduct meet the standards of the professional, ethical and relevant legislated requirements."
                }
                title={"Competency"}
                number="1.1"
                competencyData={competencies[0]}
                pdrpToReview={match.params.id}
                disabled={this.state.disabled}
              />
              <Competency
                description={
                  "Demonstrates the ability to apply the principles of Te Tiriti o Waitangi to nursing practice."
                }
                title={"Competency"}
                number="1.2"
                competencyData={competencies[1]}
                pdrpToReview={match.params.id}
                disabled={this.state.disabled}
              />
              <Competency
                description={
                  "Demonstrates accountability for directing, monitoring and evaluating nursing care that is provided by enrolled nurses and others."
                }
                title={"Competency"}
                number="1.3"
                competencyData={competencies[2]}
                pdrpToReview={match.params.id}
                disabled={this.state.disabled}
              />
              <Competency
                description={
                  "Promotes an environment that enables client safety, independence, quality of life and health."
                }
                title={"Competency"}
                number="1.4"
                competencyData={competencies[3]}
                pdrpToReview={match.params.id}
                disabled={this.state.disabled}
              />
              <Competency
                description={
                  "Practises nursing in a manner that client determines as being culturally safe."
                }
                title={"Competency"}
                number="1.5"
                competencyData={competencies[4]}
                pdrpToReview={match.params.id}
                disabled={this.state.disabled}
              />
            </CompetencyGroupPukengaHaumanu>
            <CompetencyGroupPukengaHaumanu
              title={"Domain 2: Management of Nursing Care"}
              subtitle={
                "This domain contains competencies related to client assessment and managing client care, which is responsive to client needs, and is supported by nursing knowledge and evidence based research."
              }
            >
              <Competency
                description={
                  "Provides planned nursing care to achieve identified outcomes."
                }
                title={"Management of Nursing Care"}
                number="2.1"
                competencyData={competencies[5]}
                pdrpToReview={match.params.id}
                disabled={this.state.disabled}
              />
              <Competency
                description={
                  "Undertakes a comprehensive and accurate nursing assessment of clients in a variety of settings."
                }
                title={"Management of Nursing Care"}
                number="2.2"
                competencyData={competencies[6]}
                pdrpToReview={match.params.id}
                disabled={this.state.disabled}
              />
              <Competency
                description={
                  "Ensures documentation is accurate and maintains confidentiality of information."
                }
                title={"Management of Nursing Care"}
                number="2.3"
                competencyData={competencies[7]}
                pdrpToReview={match.params.id}
                disabled={this.state.disabled}
              />
              <Competency
                description={
                  "Ensures the client has adequate explanation of the effects, consequences and alternatives of proposed treatment options."
                }
                title={"Management of Nursing Care"}
                number="2.4"
                competencyData={competencies[8]}
                pdrpToReview={match.params.id}
                disabled={this.state.disabled}
              />
              <Competency
                description={
                  "Acts appropriately to protect oneself and others when faced with unexpected client responses, confrontation, personal threat or other crisis situations."
                }
                title={"Management of Nursing Care"}
                number="2.5"
                competencyData={competencies[9]}
                pdrpToReview={match.params.id}
                disabled={this.state.disabled}
              />
              <Competency
                description={
                  "Evaluates client’s progress toward expected outcomes in partnership with clients."
                }
                title={"Management of Nursing Care"}
                number="2.6"
                competencyData={competencies[10]}
                pdrpToReview={match.params.id}
                disabled={this.state.disabled}
              />
              <Competency
                description={
                  "Provides health education appropriate to the needs of the client within a nursing framework."
                }
                title={"Management of Nursing Care"}
                number="2.7"
                competencyData={competencies[11]}
                pdrpToReview={match.params.id}
                disabled={this.state.disabled}
              />
              <Competency
                description={
                  "Reflects upon, and evaluates with peers and experienced nurses, the effectiveness of nursing care."
                }
                title={"Management of Nursing Care"}
                number="2.8"
                competencyData={competencies[12]}
                pdrpToReview={match.params.id}
                disabled={this.state.disabled}
              />
              <Competency
                description={"Maintains professional development."}
                title={"Management of Nursing Care"}
                number="2.9"
                competencyData={competencies[13]}
                pdrpToReview={match.params.id}
                disabled={this.state.disabled}
              />
            </CompetencyGroupPukengaHaumanu>
            <CompetencyGroupPukengaHaumanu
              title={"Domain 3: Interpersonal relationships"}
              subtitle={
                "This domain contains competencies related to interpersonal and therapeutic communication with client, other nursing staff and interprofessional communication and documentation."
              }
            >
              <Competency
                description={
                  "Establishes, maintains and concludes therapeutic interpersonal relationships with clients."
                }
                title={"Interpersonal relationships"}
                number="3.1"
                competencyData={competencies[14]}
                pdrpToReview={match.params.id}
                disabled={this.state.disabled}
              />
              <Competency
                description={
                  "Practises nursing in a negotiated partnership with clients where and when possible."
                }
                title={"Interpersonal relationships"}
                number="3.2"
                competencyData={competencies[15]}
                pdrpToReview={match.params.id}
                disabled={this.state.disabled}
              />
              <Competency
                description={
                  "Communicates effectively with clients and members of the health care team."
                }
                title={"Interpersonal relationships"}
                number="3.3"
                competencyData={competencies[16]}
                pdrpToReview={match.params.id}
                disabled={this.state.disabled}
              />
            </CompetencyGroupPukengaHaumanu>
            <CompetencyGroupPukengaHaumanu
              title={
                "Domain 4: Interprofessional health care and quality improvement"
              }
              subtitle={
                "This domain contains competencies to demonstrate that, as a member of the health care team; the nurse evaluates the effectiveness of care and promotes a nursing perspective within the interprofessional activities of the team."
              }
            >
              <Competency
                description={
                  "Collaborates and participates with colleagues and members of the heath care team to facilitate and coordinate care."
                }
                title={"Interprofessional health care and quality improvement"}
                number="4.1"
                competencyData={competencies[17]}
                pdrpToReview={match.params.id}
                disabled={this.state.disabled}
              />
              <Competency
                description={
                  "Recognises and values the roles and skills of all members of the health care team in the delivery of care."
                }
                title={"Interprofessional health care and quality improvement"}
                number="4.2"
                competencyData={competencies[18]}
                pdrpToReview={match.params.id}
                disabled={this.state.disabled}
              />
              <Competency
                description={
                  "Participates in quality improvement activities to monitor and improve standards of nursing."
                }
                title={"Interprofessional health care and quality improvement"}
                number="4.3"
                competencyData={competencies[19]}
                pdrpToReview={match.params.id}
                disabled={this.state.disabled}
              />
            </CompetencyGroupPukengaHaumanu>
            {this.state.value === 4 && (
              <CompetencyGroupPukengaHaumanu
                title={"Domain 5: Nurse Leadership"}
                subtitle={
                  "This domain contains competencies recommended by Nursing Council of NZ for Nurses in various leadership roles."
                }
              >
                <Competency
                  description={
                    "Promotes an environment that contributes to ongoing demonstration and evaluation of competencies. "
                  }
                  title={"Nurse Leadership 5.1"}
                  number="5.1"
                  competencyData={competencies[20]}
                  pdrpToReview={match.params.id}
                  disabled={this.state.disabled}
                />
                <Competency
                  description={
                    "Promotes a quality practice environment that supports nurses’ ability to provide safe, effective and ethical nursing practice."
                  }
                  title={"Nurse Leadership 5.2"}
                  number="5.2"
                  competencyData={competencies[21]}
                  pdrpToReview={match.params.id}
                  disabled={this.state.disabled}
                />
                <Competency
                  description={
                    "Promotes a quality practice environment that encourages learning and evidence-based practice."
                  }
                  title={"Nurse Leadership 5.3"}
                  number="5.3"
                  competencyData={competencies[22]}
                  pdrpToReview={match.params.id}
                  disabled={this.state.disabled}
                />
                <Competency
                  description={
                    "Participates in professional activities to keep abreast of current trends and issues in nursing."
                  }
                  title={"Nurse Leadership 5.4"}
                  number="5.4"
                  competencyData={competencies[23]}
                  pdrpToReview={match.params.id}
                  disabled={this.state.disabled}
                />
                <Competency
                  description={
                    "Establishes and maintains effective interpersonal relationships with others, including utilising effective interviewing and counseling skills and establishing rapport and trust."
                  }
                  title={"Nurse Leadership 5.5"}
                  number="5.5"
                  competencyData={competencies[24]}
                  pdrpToReview={match.params.id}
                  disabled={this.state.disabled}
                />
                <Competency
                  description={
                    "Communicates effectively with members of the health care team, including using a variety of effective communication techniques, employing appropriate language to context and providing adequate time for discussion."
                  }
                  title={"Nurse Leadership 5.6"}
                  number="5.6"
                  competencyData={competencies[25]}
                  pdrpToReview={match.params.id}
                  disabled={this.state.disabled}
                />
              </CompetencyGroupPukengaHaumanu>
            )}
            {!match.params.id && (
              <CardGroup
                title={"Peer Reviewer"}
                subtitle={
                  "Invite a peer. This nurse needs to be in an equal or senior role to you."
                }
              >
                {!pdrpPeer && (
                  <Alert>
                    Once you invite a peer reviewer, you will not be able to
                    change your Pukenga Haumanu entries. Please ensure you have
                    completed the competencies above before filling in the form
                    below.
                  </Alert>
                )}
                <Reviewer pdrpId={pakehaPdrpId} peer={pdrpPeer} />
              </CardGroup>
            )}
          </div>
        )}
        {match.params.id && (
          <CardGroup
            title={"Mark as Complete"}
            subtitle={"Mark this peer review as complete"}
          >
            <Alert>
              Please be sure you have completed reviewing all the competencies
              above before clicking the button below.
            </Alert>
            <RaisedButton
              label="Click to Complete Peer Review"
              primary={true}
              onClick={event => {
                peerPdrpComplete({
                  api_token: apiToken,
                  pdrp_id: competencies[0].pdrp_id
                });
              }}
            />
          </CardGroup>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    login: state.login,
    apiToken: state.login.loginStatus.response.data.api_token,
    userId: state.login.loginStatus.response.data.id,
    userRole: state.login.loginStatus.response.data.roles,
    competencies: state.competency.competencies[1]
      ? state.competency.competencies[1].competencies
      : state.competency.competencies[1],
    statusDialog: state.statusDialog.display,
    statusDialogMessage: state.statusDialog.message,
    pakehaPdrpId: state.pdrp.pdrpExist.response[1]
      ? state.pdrp.pdrpExist.response[1].id
      : state.pdrp.pdrpExist.response[1],
    maoriPdrpId: state.pdrp.pdrpExist.response[0]
      ? state.pdrp.pdrpExist.response[0].id
      : state.pdrp.pdrpExist.response[0],
    punaId: state.competency.competencies[1]
      ? state.competency.competencies[1].competencies[0].puna_id
      : state.competency.competencies[1],
    pdrpPeer: state.pdrp.pdrpPeers.response
      ? state.pdrp.pdrpPeers.response[1]
      : state.pdrp.pdrpPeers.response
    //registrationStatus: state.registration.registrationStatus.status
  };
};
const mapDispatchToProps = dispatch => ({
  loadCompetencies(userId) {
    dispatch(loadCompetencies(userId));
  },
  setStatusDialog(message) {
    dispatch(setStatusDialog(true, message));
  },
  updatePdrpPuna(content) {
    dispatch(updatePdrpPuna(content));
  },
  peerPdrpComplete(content) {
    dispatch(peerPdrpComplete(content));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectSheet(classStyles)(PukengaHaumanu));
