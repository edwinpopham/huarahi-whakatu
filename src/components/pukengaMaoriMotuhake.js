// @flow
import React, { Component } from "react";
import injectSheet from "react-jss";
import RaisedButton from "material-ui/RaisedButton";
import Heading from "./Heading";
import Competency from "./Competency";
import CompetencyGroup from "./CompetencyGroup";
import Reviewer from "./Reviewer";
import CardGroup from "./CardGroup";
import Dialog from "material-ui/Dialog";
import Alert from "./Alert";
import HowToChoosePuna from "./HowToChoosePuna";
import PunaCultural from "./PunaCultural";
import {
  loadCompetencies,
  setStatusDialog,
  peerPdrpComplete,
  updateCompetencyPuna,
} from "../actions/actionCreators";
import { connect } from "react-redux";

const classStyles = (theme: JssTheme) => ({});

export class PukengaMaoriMotuhake extends Component {
  constructor(props) {
    super(props);
    const { userRole, pdrpPeer } = this.props;
    this.state = {
      disabled: pdrpPeer && userRole === 1 ? true : false,
    };
  }

  componentDidMount() {
    const {
      userId,
      apiToken,
      loadCompetencies,
      setStatusDialog,
      match,
    } = this.props;
    match.params.id
      ? loadCompetencies({ user_id: match.params.id, api_token: apiToken })
      : loadCompetencies({ user_id: userId, api_token: apiToken });
    setStatusDialog("Loading competencies");
  }

  handleChange = (value, id) => {
    const { userId, apiToken, updateCompetencyPuna } = this.props;
    updateCompetencyPuna({
      puna: value,
      id,
      user_id: userId,
      api_token: apiToken,
    });
  };

  render() {
    const {
      apiToken,
      competencies,
      statusDialog,
      statusDialogMessage,
      maoriPdrpId,
      match,
      pdrpPeer,
      userRole,
      peerPdrpComplete,
    } = this.props;

    return (
      <div>
        <Dialog modal={false} open={statusDialog}>
          <p>{statusDialogMessage}</p>
        </Dialog>
        {apiToken !== undefined && (
          <div>
            <Heading>Pukenga Māori Motuhake</Heading>
            <HowToChoosePuna />
            {!competencies && (
              <p>You need to start your pdrp before filling in competencies.</p>
            )}
            {pdrpPeer && userRole && (
              <Alert>
                This PDRP is currently being peer reviewed. Competency editing
                is disabled.
              </Alert>
            )}
            {competencies && (
              <div>
                <CompetencyGroup
                  title={"1. Wairuatanga"}
                  subtitle={
                    "Wairuatanga influences the way people relate to each other and to the surrounding environment. Wairuatanga is more than just Karakia; although karakia aims to strengthen taha wairua, taha whānau, taha hinengaro and taha tinana. The Māori nurse demonstrates an understanding and incorporation of taha wairua as an integral part of practice."
                  }
                >
                  <PunaCultural
                    handleChange={this.handleChange}
                    competencyData={competencies[0]}
                  />
                  <Competency
                    description={
                      "Acknowledges that concepts and perceptions of Māori wairua differ from one person to another and actively seeks guidance on the nature of the experience. Expresses self-awareness of own wairua including needs amongst colleagues. Understands the place of different karakia and accesses appropriate persons to lead karakia."
                    }
                    title={"Wairuatanga"}
                    number=""
                    competencyData={competencies[0]}
                    pdrpToReview={match.params.id}
                    disabled={this.state.disabled}
                  />
                </CompetencyGroup>
                <CompetencyGroup
                  title={"2. Pupuri ki te Arikitanga"}
                  subtitle={
                    "It is important the Māori nurse understands and practices in adherence to Māori beliefs and values that maintain a balance and minimise risk."
                  }
                >
                  <PunaCultural
                    handleChange={this.handleChange}
                    competencyData={competencies[3]}
                  />
                  <Competency
                    description={
                      "Understands the concepts of tapu and noa, tika, pono and aroha and how these impact on practice with tangata whaiora and their whānau."
                    }
                    title={"Pupuri ki te Arikitanga"}
                    number=""
                    competencyData={competencies[3]}
                    pdrpToReview={match.params.id}
                    disabled={this.state.disabled}
                  />
                </CompetencyGroup>
                <CompetencyGroup
                  title={"3. Tuakiri"}
                  subtitle={
                    "The Māori nurse recognises the importance of a sense of belonging and identity; and incorporates these principles into practice."
                  }
                >
                  <PunaCultural
                    handleChange={this.handleChange}
                    competencyData={competencies[4]}
                  />
                  <Competency
                    description={
                      "Demonstrates self awareness through pepeha and whakapapa."
                    }
                    title={"Tuakiri"}
                    number=""
                    competencyData={competencies[4]}
                    pdrpToReview={match.params.id}
                    disabled={this.state.disabled}
                  />
                </CompetencyGroup>
                <CompetencyGroup
                  title={"4. Te Reo me ōna tikanga"}
                  subtitle={
                    "Māori nursing requires a high level of communication skills; advantageous are a knowledge of Te Reo and Tikanga. These enable the Māori nurse to relate to client and whānau within a Māori context."
                  }
                >
                  <PunaCultural
                    handleChange={this.handleChange}
                    competencyData={competencies[5]}
                  />
                  <Competency
                    description={
                      "Acknowledges the significance of te reo Māori in practice and uses correct pronunciation. Understands tikanga Māori and is able to validate this with knowledgeable others."
                    }
                    title={"Te Reo me ōna tikanga"}
                    number=""
                    competencyData={competencies[5]}
                    pdrpToReview={match.params.id}
                    disabled={this.state.disabled}
                  />
                </CompetencyGroup>
                <CompetencyGroup
                  title={"5. Whakawhanaungatanga"}
                  subtitle={
                    "Whakawhanaungatanga is viewed as a Māori process of building a relationship through the strengthening of kinship ties. This deliberate process promotes a connectedness and foundation for the culturally therapeutic relationship. The Māori nurse understands this process and purposely utilises processes to work in partnership with client and whānau."
                  }
                >
                  <PunaCultural
                    handleChange={this.handleChange}
                    competencyData={competencies[7]}
                  />
                  <Competency
                    description={
                      "Recognises the diversity of Māori whānau. Demonstrates a whakapapa awareness of tangata whaiora and their whānau; colleagues and manuhiri. Understands the roles of tuakana and teina; mataamua and potiki; kaumatua; whangai and other roles specific to whānau."
                    }
                    title={"Whakawhanaungatanga"}
                    number=""
                    competencyData={competencies[7]}
                    pdrpToReview={match.params.id}
                    disabled={this.state.disabled}
                  />
                </CompetencyGroup>
                <CompetencyGroup
                  title={"6. Hauora Māori"}
                  subtitle={
                    "Hauora Māori relies upon a number of approaches that address client needs in a comprehensive way. The Māori Nurse will utilise Māori models of practice placing Hauora in a broad and holistic context."
                  }
                >
                  <PunaCultural
                    handleChange={this.handleChange}
                    competencyData={competencies[10]}
                  />
                  <Competency
                    description={
                      "Recognises Māori models of practice in the assessment, planning, implementation and evaluation of care in partnership with tangata whaiora."
                    }
                    title={"Hauora Māori"}
                    number=""
                    competencyData={competencies[10]}
                    pdrpToReview={match.params.id}
                    disabled={this.state.disabled}
                  />
                </CompetencyGroup>
                {!match.params.id && (
                  <CardGroup
                    title={"Tuakana"}
                    subtitle={
                      "The expectation is that Māori knowledgeable in matauranga Maori will provide an evaluation of the nurse based on the cultural competencies."
                    }
                  >
                    {!pdrpPeer && (
                      <Alert>
                        Once you invite a Tuakana, you will not be able to
                        change your Pukenga Māori Motuhake entries. Please
                        ensure you have completed the competencies above before
                        filling in the form below.
                      </Alert>
                    )}
                    <Reviewer pdrpId={maoriPdrpId} peer={pdrpPeer} />
                  </CardGroup>
                )}
              </div>
            )}
            {match.params.id && (
              <CardGroup
                title={"Mark as Complete"}
                subtitle={"Mark this peer review as complete"}
              >
                <Alert>
                  Please be sure you have completed reviewing all the
                  competencies above before clicking the button below.
                </Alert>
                <RaisedButton
                  label="Click to Complete Peer Review"
                  primary={true}
                  onClick={(event) => {
                    peerPdrpComplete({
                      api_token: apiToken,
                      pdrp_id: competencies[0].pdrp_id,
                    });
                  }}
                />
              </CardGroup>
            )}
          </div>
        )}
        {apiToken === undefined && (
          <p>Sorry you must be logged in to view this page.</p>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    login: state.login,
    apiToken: state.login.loginStatus.response.data.api_token,
    userId: state.login.loginStatus.response.data.id,
    userRole: state.login.loginStatus.response.data.roles,
    competencies: state.competency.competencies[0]
      ? state.competency.competencies[0].competencies
      : state.competency.competencies[0],
    statusDialog: state.statusDialog.display,
    statusDialogMessage: state.statusDialog.message,
    maoriPdrpId: state.pdrp.pdrpExist.response[0]
      ? state.pdrp.pdrpExist.response[0].id
      : state.pdrp.pdrpExist.response[0],
    pdrpPeer: state.pdrp.pdrpPeers.response
      ? state.pdrp.pdrpPeers.response[2]
      : state.pdrp.pdrpPeers.response,
    //registrationStatus: state.registration.registrationStatus.status
  };
};
const mapDispatchToProps = (dispatch) => ({
  loadCompetencies(userId) {
    dispatch(loadCompetencies(userId));
  },
  setStatusDialog(message) {
    dispatch(setStatusDialog(true, message));
  },
  peerPdrpComplete(content) {
    dispatch(peerPdrpComplete(content));
  },
  updateCompetencyPuna(content) {
    dispatch(updateCompetencyPuna(content));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectSheet(classStyles)(PukengaMaoriMotuhake));
