import React, { Component } from "react";
import injectSheet from "react-jss";
import { connect } from "react-redux";
import Heading from "./Heading";
import MenuItem from "material-ui/MenuItem";
import ErrorContainer from "./ErrorContainer";
import RaisedButton from "material-ui/RaisedButton";
import DatePicker from "material-ui/DatePicker";
import {
  requestRegistration,
  setErrorMessage
} from "../actions/actionCreators";
import GooglePlaceAutocomplete from "material-ui-places";
import FlatButton from "material-ui/FlatButton";
import {
  extractFirstname,
  extractMiddlenames,
  extractLastname
} from "../helpers/nameExtractions";
import {
  extractAddress,
  extractSuburb,
  extractCity
} from "../helpers/addressExtractions";
import { APPLY_AT_LARGE_SIZE, LARGE_SIZE } from "../constants/breakPoints";
import MediaQuery from "react-responsive";
import Dialog from "material-ui/Dialog";
import {
  TextValidator,
  ValidatorForm,
  SelectValidator
} from "react-material-ui-form-validator";
import { Redirect } from "react-router";
import moment from "moment";

const classStyles = (theme: JssTheme) => ({
  row: {
    [APPLY_AT_LARGE_SIZE]: {
      display: "flex"
    }
  },
  column: {
    marginTop: "40px",
    [APPLY_AT_LARGE_SIZE]: {
      marginTop: "0",
      flexDirection: "row",
      flexGrow: "1",
      flexBasis: "0",
      borderLeft: "1px dashed #eee",
      padding: "0 20px"
    }
  },
  subHeading: {
    fontWeight: "bold",
    marginBottom: "0"
  },
  addressField: {},
  addressClick: {
    width: "100px",
    height: "100px"
  }
});

class Registration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fullName: "",
      title: "",
      homeAddress: "",
      workAddress: "",
      mobileNumber: "",
      emailAddress: "",
      employer: "",
      workPhone: "",
      dob: {},
      nurse_registration: "",
      ethnicity: "",
      iwi: "",
      hapu: "",
      open: false,
      openErrorDialog: false,
      dialogName: "",
      addressLine: "",
      suburbLine: "",
      cityLine: "",
      workAddressLine: "",
      workSuburbLine: "",
      workCityLine: "",
      errors: {}
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleAddress = this.handleAddress.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleOpen = this.handleOpen.bind(this);
  }

  componentDidMount() {
    const { setErrorMessage } = this.props;
    setErrorMessage(false);
  }

  ucFirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  handleOpen() {
    this.setState({ open: true });
  }

  handleClose() {
    this.setState({
      open: false,
      openErrorDialog: false
    });
  }

  handleChange(event, index, value, name) {
    switch (name) {
      case "title":
        this.setState({
          [name]: value
        });
        break;
      case "dob":
        this.setState({
          [name]: value
        });
        break;
      default:
        this.setState({
          [name]: event.target.value
        });
        break;
    }
  }

  handleAddress(addressType) {
    this.setState({
      dialogName: addressType,
      open: true
    });
  }

  handleErrors(error) {
    this.setState({
      openErrorDialog: true,
      errors: error
    });
  }

  errorDialog() {
    const actions = [
      <FlatButton label="Close" primary={true} onClick={this.handleClose} />
    ];
    return (
      <Dialog modal={false} actions={actions} open={this.state.openErrorDialog}>
        <p>You missed some required fields</p>
      </Dialog>
    );
  }

  redirectOnSuccess() {
    const { registrationStatus } = this.props;
    if (registrationStatus === "success") {
      return <Redirect to="/success" push={true} />;
    }
  }

  render() {
    const {
      classes,
      requestRegistration,
      statusDialog,
      statusDialogMessage
    } = this.props;

    return (
      <div>
        <ErrorContainer />
        <Dialog modal={false} open={statusDialog}>
          <p>{statusDialogMessage}</p>
        </Dialog>
        <Heading>Register</Heading>
        <p>
          By completing this form your will be enrolled in Huarahi Whakatū which
          is an on-line Nursing Council Accredited Professional Development and
          Recognition Programme (PDRP) specifically tailored by, and for, Māori
          Registered Nurses. You will receive a confirmation email, and we will
          contact you in the near future with information on how to complete
          your accreditation.
        </p>
        <ValidatorForm
          ref="form"
          onError={errors => this.handleErrors(errors)}
          onSubmit={event =>
            requestRegistration({
              title: this.state.title,
              first_name: extractFirstname(this.state.fullName),
              middle_names: extractMiddlenames(this.state.fullName),
              surname: extractLastname(this.state.fullName),
              address: this.state.homeAddress
                ? extractAddress(this.state.homeAddress)
                : this.state.addressLine,
              suburb: this.state.homeAddress
                ? extractSuburb(this.state.homeAddress)
                : this.state.suburbLine,
              city: this.state.homeAddress
                ? extractCity(this.state.homeAddress)
                : this.state.cityLine,
              country: "New Zealand",
              mobile: this.state.mobileNumber,
              email: this.state.emailAddress,
              employer: this.state.employer,
              employer_phone: this.state.workPhone,
              employer_address: this.state.workAddress
                ? extractAddress(this.state.workAddress)
                : this.state.workAddressLine,
              employer_suburb: this.state.workAddress
                ? extractSuburb(this.state.workAddress)
                : this.state.workSuburbLine,
              employer_city: this.state.workAddress
                ? extractCity(this.state.workAddress)
                : this.state.workCityLine,
              employer_country: "New Zealand",
              dob: moment(this.state.dob).format('YYYY-MM-DD'),
              nurse_registration: this.state.nurse_registration,
              ethnicity: this.state.ethnicity,
              hapu: this.state.hapu,
              iwi: this.state.iwi
            })
          }
        >
          <div className={classes.row}>
            {this.errorDialog()}
            {this.redirectOnSuccess()}
            <div className={classes.column}>
              <p className={classes.subHeading}>1. Contact Details</p>
              <SelectValidator
                name="Title"
                floatingLabelText="Title"
                value={this.state.title}
                onChange={(event, index, value) =>
                  this.handleChange(event, index, value, "title")
                }
                title="title"
                fullWidth={true}
                validators={["required"]}
                errorMessages={["this field is required"]}
                className="test-title"
              >
                <MenuItem value={0} primaryText="Please Choose" />
                <MenuItem value={"Mr."} primaryText="Mr." />
                <MenuItem value={"Ms."} primaryText="Ms." className="test-titleMs" />
                <MenuItem value={"Mrs."} primaryText="Mrs." />
                <MenuItem value={"Dr."} primaryText="Dr." />
                <MenuItem value={"Prof."} primaryText="Prof." />
                <MenuItem value={"Miss."} primaryText="Miss." />
              </SelectValidator>
              <br />
              <TextValidator
                name="fullName"
                hintText="First Middle Surname"
                floatingLabelText="Full Name"
                value={this.state.fullName}
                onChange={(event, index, value) =>
                  this.handleChange(event, index, value, "fullName")
                }
                validators={["required"]}
                errorMessages={["this field is required"]}
                fullWidth={true}
                className="test-fullName"
              />
              <br />
              <MediaQuery maxWidth={LARGE_SIZE - 1}>
                <TextValidator
                  name="homeAddressField"
                  hintText="23 Smith Road"
                  floatingLabelText="Home Street Address"
                  fullWidth={true}
                  value={this.state.addressLine}
                  onChange={(event, index, value) =>
                    this.handleChange(event, index, value, "addressLine")
                  }
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
                <TextValidator
                  name="homeSuburbField"
                  hintText="Henderson"
                  floatingLabelText="Home Suburb"
                  fullWidth={true}
                  value={this.state.suburbLine}
                  onChange={(event, index, value) =>
                    this.handleChange(event, index, value, "suburbLine")
                  }
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
                <TextValidator
                  name="homeCityField"
                  hintText="Auckland"
                  floatingLabelText="Home City"
                  fullWidth={true}
                  value={this.state.cityLine}
                  onChange={(event, index, value) =>
                    this.handleChange(event, index, value, "cityLine")
                  }
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
              </MediaQuery>
              <MediaQuery minWidth={LARGE_SIZE}>
                <GooglePlaceAutocomplete
                  searchText={this.state.homeAddress}
                  onChange={searchText => {
                    this.setState({
                      homeAddress: searchText.target.value
                    });
                  }}
                  onNewRequest={() => {}}
                  name={"homeaddress"}
                  floatingLabelText="Home Address"
                  multiLine={false}
                  restrictions={{ country: "nz" }}
                  maxSearchResults={2}
                  fullWidth={true}
                  className="test-homeAddress"
                />
              </MediaQuery>
              <br />
              <TextValidator
                name="mobileNumber"
                hintText="Mobile number"
                floatingLabelText="Mobile"
                value={this.state.mobileNumber}
                onChange={(event, index, value) =>
                  this.handleChange(event, index, value, "mobileNumber")
                }
                validators={["required"]}
                errorMessages={["this field is required"]}
                fullWidth={true}
                className="test-mobile"
              />
              <br />
              <TextValidator
                name="emailAddress"
                hintText="Email Address"
                floatingLabelText="Email Address"
                value={this.state.emailAddress}
                onChange={(event, index, value) =>
                  this.handleChange(event, index, value, "emailAddress")
                }
                validators={["required", "isEmail"]}
                errorMessages={["this field is required", "email is not valid"]}
                fullWidth={true}
                className="test-email"
              />
            </div>
            <div className={classes.column}>
              <p className={classes.subHeading}>2. Employer Details</p>
              <TextValidator
                name="employer"
                hintText="Name of Employer"
                floatingLabelText="Name of Employer"
                value={this.state.employer}
                onChange={(event, index, value) =>
                  this.handleChange(event, index, value, "employer")
                }
                validators={["required"]}
                errorMessages={["this field is required"]}
                fullWidth={true}
              />
              <br />
              <TextValidator
                name="workPhone"
                hintText="Work Phone"
                floatingLabelText="Work Phone"
                value={this.state.workPhone}
                onChange={(event, index, value) =>
                  this.handleChange(event, index, value, "workPhone")
                }
                validators={["required"]}
                errorMessages={["this field is required"]}
                fullWidth={true}
              />
              <br />
              <MediaQuery maxWidth={LARGE_SIZE - 1}>
                <TextValidator
                  name="workAddressField"
                  hintText="23 Smith Road"
                  floatingLabelText="Work Street Address"
                  fullWidth={true}
                  value={this.state.workAddressLine}
                  onChange={(event, index, value) =>
                    this.handleChange(event, index, value, "workAddressLine")
                  }
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
                <TextValidator
                  name="workSuburbField"
                  hintText="Henderson"
                  floatingLabelText="Work Suburb"
                  fullWidth={true}
                  value={this.state.workSuburbLine}
                  onChange={(event, index, value) =>
                    this.handleChange(event, index, value, "workSuburbLine")
                  }
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
                <TextValidator
                  name="workCityField"
                  hintText="Auckland"
                  floatingLabelText="Work City"
                  fullWidth={true}
                  value={this.state.workCityLine}
                  onChange={(event, index, value) =>
                    this.handleChange(event, index, value, "workCityLine")
                  }
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
              </MediaQuery>
              <MediaQuery minWidth={LARGE_SIZE}>
                <GooglePlaceAutocomplete
                  searchText={this.state.workAddress}
                  onChange={searchText => {
                    this.setState({
                      workAddress: searchText.target.value
                    });
                  }}
                  onNewRequest={() => {}}
                  name={"workaddress"}
                  floatingLabelText="Work Address"
                  multiLine={true}
                  restrictions={{ country: "nz" }}
                  maxSearchResults={2}
                  fullWidth={true}
                />
              </MediaQuery>
            </div>
            <div className={classes.column}>
              <p className={classes.subHeading}>3. Personal Details</p>
              <TextValidator
                name="nurse_registration"
                hintText="Nursing Council Registration Number"
                floatingLabelText="Registration number"
                value={this.state.nurse_registration}
                onChange={(event, index, value) =>
                  this.handleChange(event, index, value, "nurse_registration")
                }
                validators={["required"]}
                errorMessages={["this field is required"]}
                fullWidth={true}
              />
              <br />
              <DatePicker
                name="dob"
                hintText="Date of Birth"
                openToYearSelection={true}
                value={this.state.dob}
                onChange={(event, date) =>
                  this.handleChange(null, null, date, "dob")
                }
              />
              <br />
              <TextValidator
                name="ethnicity"
                hintText="Use a comma for multiple"
                floatingLabelText="Your Ethnicity"
                value={this.state.ethnicity}
                onChange={(event, index, value) =>
                  this.handleChange(event, index, value, "ethnicity")
                }
                validators={["required"]}
                errorMessages={["this field is required"]}
                fullWidth={true}
              />
              <br />
              <TextValidator
                name="iwi"
                hintText="Use a comma for multiple"
                floatingLabelText="Your Iwi"
                value={this.state.iwi}
                onChange={(event, index, value) =>
                  this.handleChange(event, index, value, "iwi")
                }
                validators={["required"]}
                errorMessages={["this field is required"]}
                fullWidth={true}
              />
              <br />
              <TextValidator
                name="hapu"
                hintText="Use a comma for multiple"
                floatingLabelText="Your Hapu"
                value={this.state.hapu}
                onChange={(event, index, value) =>
                  this.handleChange(event, index, value, "hapu")
                }
                validators={["required"]}
                errorMessages={["this field is required"]}
                fullWidth={true}
              />
              <p>
                As part of this Professional Development Recognition Programme
                you agree to inform Te Rau Matatini of any of the following:
              </p>

              <ul>
                <li>Change of Employee/er</li>

                <li>Change of Contact Details</li>

                <li>Receipt of updated Annual Practicing Cerificate (APC)</li>
              </ul>
              <RaisedButton label="Submit" primary={true} type="submit" />
            </div>
          </div>
        </ValidatorForm>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    registrationStatus: state.registration.status,
    statusDialog: state.statusDialog.display,
    statusDialogMessage: state.statusDialog.message
  };
};
const mapDispatchToProps = dispatch => ({
  requestRegistration(registrationDetails) {
    dispatch(requestRegistration(registrationDetails));
  },
  setErrorMessage(display) {
    dispatch(setErrorMessage(display));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectSheet(classStyles)(Registration));
