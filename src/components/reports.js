// @flow
import React, { Component } from "react";
import injectSheet from "react-jss";
import Heading from "./Heading";
import { Bar } from "react-chartjs";

const classStyles = (theme: JssTheme) => ({});

export class Reports extends Component {
  render() {
    const data = {
      labels: ["January", "February", "March", "April", "May", "June", "July"],
      datasets: [
        {
          label: "Last Year",
          fillColor: "rgba(220,220,220,0.8)",
          strokeColor: "rgba(220,220,220,0.8)",
          highlightFill: "rgba(220,220,220,0.5)",
          highlightStroke: "rgba(220,220,220,0.5)",
          data: [65, 59, 80, 81, 56, 55, 40]
        },
        {
          label: "This Year",
          fillColor: "rgba(33,150,243,1)",
          strokeColor: "rgba(33,150,243,1)",
          highlightFill: "rgba(33,150,243,0.75)",
          highlightStroke: "rgba(33,150,243,0.75)",
          data: [28, 48, 40, 19, 86, 27, 90]
        }
      ]
    };

    const options = {

    };

    return (
      <div>
        <Heading>Reports</Heading>
        <Bar data={data} options={options} width="600" height="300" />
        <p>
          Registrations this month, year on year.
        </p>
      </div>
    );
  }
}

export default injectSheet(classStyles)(Reports);
