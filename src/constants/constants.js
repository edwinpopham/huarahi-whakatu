export const REQUEST_ARTICLES = 'REQUEST_ARTICLES';
export const RECEIVE_ARTICLES = 'RECEIVE_ARTICLES';
export const FETCH_POSTS_REQUEST = 'FETCH_POSTS_REQUEST';
export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';
export const FETCH_POSTS_FAILURE = 'FETCH_POSTS_FAILURE';
export const POST_REGISTRATION = 'POST_REGISTRATION';
export const RECEIVE_REGISTRATION_RESPONSE = 'RECEIVE_REGISTRATION_RESPONSE';
export const SET_REGISTRATION_STATUS_DIALOG = 'SET_REGISTRATION_STATUS_DIALOG';
export const REQUEST_LOGIN = 'REQUEST_LOGIN';
export const REQUEST_LOGOUT = 'REQUEST_LOGOUT';
export const SET_LOGIN_STATUS_DIALOG = 'SET_LOGIN_STATUS_DIALOG';
export const RECEIVE_LOGIN_RESPONSE = 'RECEIVE_LOGIN_RESPONSE';
export const RECEIVE_LOGOUT_RESPONSE = 'RECEIVE_LOGOUT_RESPONSE';
export const REQUEST_PDRP_START = 'REQUEST_PDRP_START';
export const RECEIVE_PDRP_START = 'RECEIVE_PDRP_START';
export const RECEIVE_PDRP_EXIST = 'RECEIVE_PDRP_EXIST';
export const SET_LOAD_COMPETENCIES_STATUS_DIALOG = 'SET_LOAD_COMPETENCIES_STATUS_DIALOG';
export const RECEIVE_LOAD_COMPETENCIES_RESPONSE = 'RECEIVE_LOAD_COMPETENCIES_RESPONSE';
export const SET_SAVE_COMPETENCY_STATUS_DIALOG = 'SET_SAVE_COMPETENCY_STATUS_DIALOG';
export const RECEIVE_SAVE_COMPETENCY_RESPONSE = 'RECEIVE_SAVE_COMPETENCY_RESPONSE';
export const SET_STATUS_DIALOG = 'SET_STATUS_DIALOG';
export const SET_ERROR_MESSAGE = 'SET_ERROR_MESSAGE';
export const REQUEST_RESET_RECEIVED = 'REQUEST_RESET_RECEIVED';
export const RECEIVE_CREATE_EVIDENCE_RESPONSE = 'RECEIVE_CREATE_EVIDENCE_RESPONSE';
export const RECEIVE_LOAD_EVIDENCES_RESPONSE = 'RECEIVE_LOAD_EVIDENCES_RESPONSE';
export const RECEIVE_UPDATE_PDRP_PUNA = 'RECEIVE_UPDATE_PDRP_PUNA';
export const RECEIVE_PEER_REVIEWER = 'RECEIVE_PEER_REVIEWER';
export const RECEIVE_PEER_PDRP_COMPLETE = 'RECEIVE_PEER_PDRP_COMPLETE';
export const RECEIVE_ALL_USERS = 'RECEIVE_ALL_USERS';
export const SET_ASSESSOR = 'SET_ASSESSOR';
export const RECEIVE_PEER_COMPLETE_PDRPS = 'RECEIVE_PEER_COMPLETE_PDRPS';
export const ASSIGN_ASSESSOR = 'ASSIGN_ASSESSOR';
export const RECEIVE_ASSESSOR_ASSIGNED_PDRPS = 'RECEIVE_ASSESSOR_ASSIGNED_PDRPS';
export const RECEIVE_UPDATE_COMPETENCY_PUNA = 'RECEIVE_UPDATE_COMPETENCY_PUNA';
export const RECEIVE_COMPETENCY_STANDARD = 'RECEIVE_COMPETENCY_STANDARD';
