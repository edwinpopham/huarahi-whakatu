import {
  extractSuburb,
  extractCity,
  extractCountry,
  extractAddress
} from "../addressExtractions";

it("extracts a suburb", () => {
  expect(extractSuburb("31 Te Mai Road, Woodhill, Whangarei, New Zealand")).toEqual("Woodhill");
});

it("extracts a city", () => {
  expect(extractCity("31 Te Mai Road, Woodhill, Whangarei, New Zealand")).toEqual("Whangarei");
});

it("extracts a country", () => {
  expect(extractCountry("31 Te Mai Road, Woodhill, Whangarei, New Zealand")).toEqual("New Zealand");
});

it("extracts a address", () => {
  expect(extractAddress("31 Te Mai Road, Woodhill, Whangarei, New Zealand")).toEqual("31 Te Mai Road");
});

it("extracts a suburb from short address", () => {
  expect(extractSuburb("300 Queen Street, Auckland, New Zealand")).toEqual(undefined);
});

it("extracts a city from short address", () => {
  expect(extractCity("300 Queen Street, Auckland, New Zealand")).toEqual("Auckland");
});

it("extracts a country from short address", () => {
  expect(extractCountry("300 Queen Street, Auckland, New Zealand")).toEqual("New Zealand");
});

it("extracts a address from short address", () => {
  expect(extractAddress("300 Queen Street, Auckland, New Zealand")).toEqual("300 Queen Street");
});
