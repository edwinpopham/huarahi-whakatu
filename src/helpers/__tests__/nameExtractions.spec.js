import { extractFirstname, extractMiddlenames, extractLastname } from "../nameExtractions";

it("gets a first name", () => {
  expect(extractFirstname("Edwin Perry Popham")).toEqual("Edwin");
});

it("gets a middle names", () => {
  expect(extractMiddlenames("Edwin Perry Popham")).toEqual("Perry");
});

it('gets a last name', () => {
  expect(extractLastname('Edwin Perry Popham')).toEqual('Popham');
});
