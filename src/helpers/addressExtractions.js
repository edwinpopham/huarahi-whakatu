export const extractSuburb = address => {
  const addressArray = address.split(", ");
  if (addressArray.length > 3) {
    return addressArray[1];
  }
  return address;
};

export const extractCity = address => {
  const addressArray = address.split(", ");
  if (addressArray.length > 3) {
    return addressArray[2];
  }
  return address;
};

export const extractCountry = address => {
  const addressArray = address.split(", ");
  if (addressArray.length > 3) {
    return addressArray[3];
  }
  return address;
};

export const extractAddress = address => {
  const addressArray = address.split(", ");
  if (addressArray.length > 0) {
    return addressArray[0];
  }
  return address;
};
