import _ from 'underscore';

export const evidenceFilter = (evidences, evidenceType) => {

  return _.filter(evidences, function(evidence){ return evidence.evidencetypes_id === evidenceType; });
};
