export const extractFirstname = fullName => {
  const nameArray = fullName.split(" ");
  return nameArray.shift();
};

export const extractMiddlenames = fullName => {
  let nameArray = fullName.split(" ");
  nameArray.shift();
  nameArray.pop();
  return nameArray.toString();
};

export const extractLastname = fullName => {
  const nameArray = fullName.split(" ");
  return nameArray.pop();
};
