import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import { ThemeProvider } from "react-jss";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import { jssTheme, muiTheme } from "./themes";
import { store, persistor } from "./store";
import App from "./components/app";
import { BrowserRouter } from "react-router-dom";
import { PersistGate } from "redux-persist/integration/react";

const target = document.querySelector("#root");

render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <ThemeProvider theme={jssTheme}>
        <MuiThemeProvider muiTheme={muiTheme}>
          <BrowserRouter>
            <App />
          </BrowserRouter>
        </MuiThemeProvider>
      </ThemeProvider>
    </PersistGate>
  </Provider>,
  target
);
