import {
  RECEIVE_ALL_USERS,
  RECEIVE_LOGOUT_RESPONSE,
  RECEIVE_LOGIN_RESPONSE,
  RECEIVE_PEER_COMPLETE_PDRPS,
  RECEIVE_ASSESSOR_ASSIGNED_PDRPS
} from "../constants/constants";

function admin(state = {}, action) {
  switch (action.type) {
    case RECEIVE_ASSESSOR_ASSIGNED_PDRPS:
      return {
        ...state,
        assessorAssignedPdrps: action.responseJson
      };
    case RECEIVE_ALL_USERS:
      return {
        ...state,
        users: action.responseJson
      };
    case RECEIVE_PEER_COMPLETE_PDRPS:
      return {
        ...state,
        peerCompletePdrps: action.responseJson
      };
    case RECEIVE_LOGOUT_RESPONSE:
    case RECEIVE_LOGIN_RESPONSE:
      return {
        ...state,
        users: [{ user: "user" }],
        peerCompletePdrps: [
          {
            id: 1,
            user: {
              registration: {
                first_name: "first_name"
              }
            },
            type: {
              name: "name"
            }
          }
        ],
        assessorAssignedPdrps: []
      };
    default:
      return state;
  }
}

export default admin;
