import {
  RECEIVE_LOAD_COMPETENCIES_RESPONSE,
  RECEIVE_LOGOUT_RESPONSE,
  RECEIVE_LOGIN_RESPONSE
} from "../constants/constants";

function competency(state = {}, action) {
  switch (action.type) {
    case RECEIVE_LOAD_COMPETENCIES_RESPONSE:
      return {
        ...state,
        competencies: action.responseJson
      };
    case RECEIVE_LOGOUT_RESPONSE:
    case RECEIVE_LOGIN_RESPONSE:
      return {
        ...state,
        competencies: [
          {competencies: 'competencies'},
          {competencies: 'competencies'}
        ]
      };
    default:
      return state;
  }
}

export default competency;
