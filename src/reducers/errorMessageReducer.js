import {
  SET_ERROR_MESSAGE,
  REQUEST_RESET_RECEIVED
} from "../constants/constants";

function errorMessage(state = {}, action) {
  switch (action.type) {
    case SET_ERROR_MESSAGE:
      return {
        ...state,
        display: action.display,
        message: action.message,
        resetRequested: false
      };
    case REQUEST_RESET_RECEIVED:
      return {
        ...state,
        resetRequested: true
      };
    default:
      return state;
  }
}

export default errorMessage;
