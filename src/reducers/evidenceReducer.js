import {
  RECEIVE_LOAD_EVIDENCES_RESPONSE,
  RECEIVE_LOGOUT_RESPONSE,
  RECEIVE_LOGIN_RESPONSE,
  RECEIVE_CREATE_EVIDENCE_RESPONSE
} from "../constants/constants";

function competency(state = {}, action) {
  switch (action.type) {
    case RECEIVE_LOAD_EVIDENCES_RESPONSE:
    case RECEIVE_CREATE_EVIDENCE_RESPONSE:
      return {
        ...state,
        evidences: action.responseJson[0] ? action.responseJson[0].evidences : action.responseJson[0]
      };
    case RECEIVE_LOGOUT_RESPONSE:
    case RECEIVE_LOGIN_RESPONSE:
      return {
        ...state,
        evidences: [{ evidences: "evidences" }]
      };
    default:
      return state;
  }
}

export default competency;
