import { combineReducers } from "redux";
// import { routerReducer } from "react-router-redux";
import registration from "./registrationReducer";
import login from "./loginReducer";
import pdrp from "./pdrpReducer";
import competency from "./competencyReducer";
import statusDialog from "./statusDialogReducer";
import errorMessage from "./errorMessageReducer";
import evidence from "./evidenceReducer";
import admin from "./adminReducer";

export default combineReducers({
  // router: routerReducer,
  registration,
  login,
  pdrp,
  competency,
  statusDialog,
  errorMessage,
  evidence,
  admin
});
