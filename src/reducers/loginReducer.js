import {
  SET_LOGIN_STATUS_DIALOG,
  RECEIVE_LOGIN_RESPONSE,
  RECEIVE_LOGOUT_RESPONSE
} from "../constants/constants";

function login(state = {}, action) {
  switch (action.type) {
    case SET_LOGIN_STATUS_DIALOG:
      return {
        ...state,
        loginStatus: {
          ...state.loginStatus,
          dialogOpen: action.dialogOpen
        }
      };
    case RECEIVE_LOGIN_RESPONSE:
      return {
        ...state,
        loginStatus: {
          ...state.loginStatus,
          dialogOpen: false,
          status: "success",
          response: action.responseJson
        }
      };
    case RECEIVE_LOGOUT_RESPONSE:
      return {
        ...state,
        loginStatus: {
          ...state.loginStatus,
          dialogOpen: false,
          status: "loggedout",
          response: action.responseJson
        }
      };
    default:
      return state;
  }
}

export default login;
