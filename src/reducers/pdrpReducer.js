import {
  REQUEST_PDRP_START,
  RECEIVE_PDRP_START,
  RECEIVE_LOGOUT_RESPONSE,
  RECEIVE_PDRP_EXIST,
  RECEIVE_PEER_REVIEWER
} from "../constants/constants";

function pdrp(state = {}, action) {
  switch (action.type) {
    case REQUEST_PDRP_START:
      return {
        ...state,
        pdrpStartStatus: {
          ...state.pdrpStartStatus,
          dialogOpen: action.dialogOpen,
          status: "creating"
        }
      };
    case RECEIVE_PDRP_START:
      return {
        ...state,
        pdrpStartStatus: {
          ...state.pdrpStartStatus,
          dialogOpen: false,
          response: action.responseJson,
          status: "created"
        },
        pdrpExist: {
          ...state.pdrpExist,
          response: action.responseJson,
          status: action.responseJson
        }
      };
    case RECEIVE_PDRP_EXIST:
      return {
        ...state,
        pdrpExist: {
          ...state.pdrpExist,
          response: action.responseJson.pdrp,
          status: action.responseJson.pdrp
        },
        pdrpsToReview: {
          ...state.pdrpToReview,
          response: action.responseJson.peer_reviews
        },
        pdrpsToAssess: {
          ...state.pdrpsToAssess,
          response: action.responseJson.assessor_reviews
        }
      };
    case RECEIVE_PEER_REVIEWER:
      return {
        ...state,
        pdrpExist: {
          ...state.pdrpExist,
        },
        pdrpsToReview: {
          ...state.pdrpsToReview,
        },
        pdrpsToAssess: {
          ...state.pdrpsToAssess
        },
        pdrpPeers: {
          ...state.pdrpPeers,
          response: action.responseJson
        }
      };
    case RECEIVE_LOGOUT_RESPONSE:
      return {
        ...state,
        pdrpExist: {
          response: undefined,
          status: undefined
        },
        pdrpStartStatus: {
          dialogOpen: false,
          status: undefined
        },
        pdrpsToReview: {
          response: undefined
        },
        pdrpsToAssess: {
          response: undefined
        },
        pdrpPeers: {
          response: undefined
        }
      };
    default:
      return state;
  }
}

export default pdrp;
