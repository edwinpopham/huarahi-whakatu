import {
  SET_REGISTRATION_STATUS_DIALOG,
  RECEIVE_REGISTRATION_RESPONSE,
  RECEIVE_LOGOUT_RESPONSE,
  SET_ERROR_MESSAGE
} from "../constants/constants";

function registration(state = {}, action) {
  switch (action.type) {
    case SET_REGISTRATION_STATUS_DIALOG:
      return {
        ...state
      };
    case RECEIVE_REGISTRATION_RESPONSE:
      return {
        ...state,
        status: action.registrationDetails.status
      };
    case RECEIVE_LOGOUT_RESPONSE:
      return {
        ...state
      };
    case SET_ERROR_MESSAGE:
      return {
        ...state,
        status: "reset"
      };
    default:
      return state;
  }
}

export default registration;
