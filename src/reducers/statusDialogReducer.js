import {
  SET_STATUS_DIALOG,
  SET_ERROR_MESSAGE,
  REQUEST_RESET_RECEIVED
} from "../constants/constants";

function statusDialog(state = {}, action) {
  switch (action.type) {
    case SET_STATUS_DIALOG:
      return {
        ...state,
        display: action.dialog,
        message: action.message
      };
    case SET_ERROR_MESSAGE:
    case REQUEST_RESET_RECEIVED:
      return {
        ...state,
        display: false,
        message: "none"
      };
    default:
      return state;
  }
}

export default statusDialog;
