import { createStore, applyMiddleware, compose } from "redux";
import { persistStore, persistReducer } from "redux-persist";
import thunk from "redux-thunk";
import storage from "redux-persist/lib/storage"; // defaults to localStorage for web and AsyncStorage for react-native

import rootReducer from "./reducers";

const persistConfig = {
  key: "root",
  storage
};

const enhancers = [];
const middleware = [thunk];

if (process.env.NODE_ENV === "development") {
  const devToolsExtension = window.devToolsExtension;

  if (typeof devToolsExtension === "function") {
    enhancers.push(devToolsExtension());
  }
}

export const initialState = {
  registration: {
    status: "not registered"
  },
  login: {
    loginStatus: {
      response: {
        data: {
          api_token: undefined
        }
      }
    }
  },
  pdrp: {
    pdrpStartStatus: {
      dialogOpen: false,
      status: "notStarted"
    },
    pdrpExist: {
      status: false
    },
    pdrpPeers: {
      response: undefined
    }
  },
  competency: {
    competencies: [
      { competencies: "competencies" },
      { competencies: "competencies" }
    ]
  },
  statusDialog: {
    display: false,
    message: "none"
  },
  errorMessage: {
    display: false,
    message: "none"
  }
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

export let store = createStore(
  persistedReducer,
  initialState,
  compose(applyMiddleware(...middleware), ...enhancers)
);
export let persistor = persistStore(store);

// import { createStore, applyMiddleware, compose } from "redux";
// import thunk from "redux-thunk";
// import rootReducer from "./reducers";

// const initialState = {
//   registration: {
//     registrationStatus: {
//       dialogOpen: false,
//       status: 'notSent'
//     }
//   }
// };
// const enhancers = [];
// const middleware = [thunk];

// if (process.env.NODE_ENV === "development") {
//   const devToolsExtension = window.devToolsExtension;

//   if (typeof devToolsExtension === "function") {
//     enhancers.push(devToolsExtension());
//   }
// }

// const composedEnhancers = compose(applyMiddleware(...middleware), ...enhancers);

// export default createStore(rootReducer, initialState, composedEnhancers);
